set(GLFW_INCLUDE_DIR ${CMAKE_BINARY_DIR}/include/GLFW ${GLUT_INCLUDE_DIR})
set(GLFW_SHARED_LIB ${CMAKE_BINARY_DIR}/lib/libglfw.so ${GLUT_SHARED_LIB})

include(CheckLibraryExists)
find_package(glfw3)
if(TARGET glfw AND NOT EXISTS ${GLFW_SHARED_LIB})
	get_target_property(GLFW_INCLUDE_DIRS glfw INTERFACE_INCLUDE_DIRECTORIES)
	set(GLFW_INCLUDE_DIR ${GLFW_INCLUDE_DIRS} ${GLUT_INCLUDE_DIR})
	set(GLFW_SHARED_LIB glfw ${GLUT_SHARED_LIB})
	add_library(glfw_lib ALIAS glfw)
	return()
endif()


add_custom_command(OUTPUT "${CMAKE_BINARY_DIR}/glfw"
	COMMAND git clone https://github.com/glfw/glfw.git
	WORKING_DIRECTORY ${CMAKE_BINARY_DIR}
)

add_custom_command(OUTPUT ${CMAKE_BINARY_DIR}/lib/libglfw.so
	COMMAND cmake -S . -B build -DCMAKE_INSTALL_PREFIX=${CMAKE_INSTALL_PREFIX} -DBUILD_SHARED_LIBS=ON -DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE}
	COMMAND cmake --build build --parallel
	COMMAND cmake --install build
	DEPENDS "${CMAKE_BINARY_DIR}/glfw"
	WORKING_DIRECTORY "${CMAKE_BINARY_DIR}/glfw"
)

add_custom_target(glfw_lib
	WORKING_DIRECTORY "${CMAKE_BINARY_DIR}/glfw"
	DEPENDS ${CMAKE_BINARY_DIR}/lib/libglfw.so
)
