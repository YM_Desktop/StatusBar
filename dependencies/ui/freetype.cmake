find_package(Freetype REQUIRED)
if(${FREETYPE_FOUND})
	set(FREETYPE_INCLUDE_DIR ${FREETYPE_INCLUDE_DIRS})
	set(FREETYPE_SHARED_LIB ${FREETYPE_LIBRARIES})
	add_library(freetype_lib ALIAS Freetype::Freetype)
	return()
endif()

set(FREETYPE_INCLUDE_DIR ${CMAKE_BINARY_DIR}/include/freetype2)
is_debug_build(IS_DEBUG)
if(${IS_DEBUG})
	set(FREETYPE_SHARED_LIB ${CMAKE_BINARY_DIR}/lib/libfreetyped.so)
else()
	set(FREETYPE_SHARED_LIB ${CMAKE_BINARY_DIR}/lib/libfreetype.so)
endif()


add_custom_command(OUTPUT "${CMAKE_BINARY_DIR}/freetype"
	COMMAND git clone https://github.com/freetype/freetype.git
	WORKING_DIRECTORY ${CMAKE_BINARY_DIR}
)

add_custom_command(OUTPUT ${FREETYPE_SHARED_LIB}
	COMMAND cmake -S . -B build -DCMAKE_INSTALL_PREFIX=${CMAKE_INSTALL_PREFIX} -DBUILD_SHARED_LIBS=ON -DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE}
	COMMAND cmake --build build --parallel
	COMMAND cmake --install build
	DEPENDS "${CMAKE_BINARY_DIR}/freetype"
	WORKING_DIRECTORY "${CMAKE_BINARY_DIR}/freetype"
)

add_custom_target(freetype_lib
	WORKING_DIRECTORY "${CMAKE_BINARY_DIR}/freetype"
	DEPENDS ${FREETYPE_SHARED_LIB}
)

