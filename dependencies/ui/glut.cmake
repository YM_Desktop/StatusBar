find_package(OpenGL REQUIRED)
find_package(GLUT REQUIRED)
if(${GLUT_FOUND})
	set(GLUT_INCLUDE_DIR ${GLUT_INCLUDE_DIRS})
	set(GLUT_SHARED_LIB ${GLUT_LIBRARIES} ${OPENGL_LIBRARIES})
	add_library(glut_lib ALIAS GLUT::GLUT)
	return()
endif()

set(GLUT_INCLUDE_DIR ${CMAKE_BINARY_DIR}/include/GL)
set(GLUT_SHARED_LIB ${CMAKE_BINARY_DIR}/lib/libglut.so ${OPENGL_LIBRARIES})

add_custom_command(OUTPUT "${CMAKE_BINARY_DIR}/freeglut"
	COMMAND git clone https://github.com/FreeGLUTProject/freeglut.git
	WORKING_DIRECTORY ${CMAKE_BINARY_DIR}
)

add_custom_command(OUTPUT ${CMAKE_BINARY_DIR}/lib/libglut.so
	COMMAND cmake -S . -B build -DCMAKE_INSTALL_PREFIX=${CMAKE_INSTALL_PREFIX} -DFREEGLUT_BUILD_SHARED_LIBS=ON -DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE}
	COMMAND cmake --build build --parallel
	COMMAND cmake --install build
	DEPENDS "${CMAKE_BINARY_DIR}/freeglut"
	WORKING_DIRECTORY "${CMAKE_BINARY_DIR}/freeglut"
)

add_custom_target(glut_lib
	WORKING_DIRECTORY "${CMAKE_BINARY_DIR}/freeglut"
	DEPENDS ${CMAKE_BINARY_DIR}/lib/libglut.so 
)

