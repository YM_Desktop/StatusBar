#pragma once
#include "ApplicationConfigBuilder.h"

#include <ApplicationConfig.h>
#include <gtest/gtest.h>

namespace valid_config_tests {
  struct TestParam {
    std::filesystem::path path;
    ApplicationConfig config;
  };

  const auto test_samples = testing::Values(
      TestParam{"data/battery_critical_level_only.yaml",
                ApplicationConfigBuilder{}
                    .set_battery_config({.critical = 10u})
                    .build()},
      TestParam{"data/battery_charging_color_only.yaml",
                ApplicationConfigBuilder{}
                    .set_battery_charging_color({0, 0, 0})
                    .build()},
      TestParam{"data/battery_critical_color_only.yaml",
                ApplicationConfigBuilder{}
                    .set_battery_critical_color({0, 0, 0})
                    .build()},
      TestParam{
          "data/battery_full_color_only.yaml",
          ApplicationConfigBuilder{}.set_battery_full_color({0, 0, 0}).build()},
      TestParam{"data/battery_normal_color_only.yaml",
                ApplicationConfigBuilder{}
                    .set_battery_normal_color({0, 0, 0})
                    .build()},
      TestParam{"data/font_path_only.yaml",
                ApplicationConfigBuilder{}
                    .set_font_config(
                        {.path = "/usr/share/fonts/TTF/RobotoMono-Thin.ttf"})
                    .build()},
      TestParam{
          "data/font_size_only.yaml",
          ApplicationConfigBuilder{}.set_font_config({.size = 10ul}).build()},
      TestParam{"data/background_color_only.yaml",
                ApplicationConfigBuilder{}.set_background({0, 0, 0}).build()},
      TestParam{"data/text_color_only.yaml",
                ApplicationConfigBuilder{}.set_text({0, 0, 0}).build()},
      TestParam{
          "data/time_format_only.yaml",
          ApplicationConfigBuilder{}.set_time_config({.format = "%c"}).build()},
      TestParam{
          "data/uptime_format_seconds_only.yaml",
          ApplicationConfigBuilder{}
              .set_uptime_config(
                  {.format = ui::UserInterfaceConfig::Uptime::Format::SECONDS})
              .build()},
      TestParam{
          "data/uptime_format_minutes_only.yaml",
          ApplicationConfigBuilder{}
              .set_uptime_config(
                  {.format = ui::UserInterfaceConfig::Uptime::Format::MINUTES})
              .build()},
      TestParam{
          "data/uptime_format_human_only.yaml",
          ApplicationConfigBuilder{}
              .set_uptime_config(
                  {.format = ui::UserInterfaceConfig::Uptime::Format::HUMAN})
              .build()},
      TestParam{"data/defaults.yaml", ApplicationConfigBuilder{}.build()},
      TestParam{
          "data/all_non_default.yaml",
          ApplicationConfigBuilder{}
              .set_battery_config({.critical = 15})
              .set_battery_critical_color({1, 2, 31})
              .set_battery_charging_color({30, 20, 100})
              .set_battery_full_color({1, 29, 9})
              .set_battery_normal_color({1, 200, 0})
              .set_text({27, 7, 27})
              .set_background({1, 2, 3})
              .set_font_config(
                  {.path = "/usr/share/fonts/TTF/RobotoMono-Italic.ttf",
                   .size = 3ul})
              .set_time_config({.format = "%c"})
              .set_uptime_config(
                  {.format = ui::UserInterfaceConfig::Uptime::Format::SECONDS})
              .build()});
} // namespace valid_config_tests
