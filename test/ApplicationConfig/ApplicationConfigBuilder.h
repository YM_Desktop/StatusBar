#pragma once
#include <ApplicationConfig.h>

class ApplicationConfigBuilder {
  ApplicationConfig m_config;

public:
  ApplicationConfigBuilder&
  set_uptime_config(ui::UserInterfaceConfig::Uptime cfg) {
    m_config.user_interface.uptime = cfg;
    return *this;
  }

  ApplicationConfigBuilder& set_time_config(ui::UserInterfaceConfig::Time cfg) {
    m_config.user_interface.time = cfg;
    return *this;
  }

  ApplicationConfigBuilder&
  set_battery_config(ui::UserInterfaceConfig::Battery cfg) {
    m_config.user_interface.battery = cfg;
    return *this;
  }

  ApplicationConfigBuilder& set_background(ui::glfw::Color c) {
    m_config.user_interface.colors.background = c;
    return *this;
  }

  ApplicationConfigBuilder& set_text(ui::glfw::Color c) {
    m_config.user_interface.colors.text = c;
    return *this;
  }

  ApplicationConfigBuilder& set_battery_normal_color(ui::glfw::Color c) {
    m_config.user_interface.battery.colors.normal = c;
    return *this;
  }

  ApplicationConfigBuilder& set_battery_full_color(ui::glfw::Color c) {
    m_config.user_interface.battery.colors.full = c;
    return *this;
  }

  ApplicationConfigBuilder& set_battery_critical_color(ui::glfw::Color c) {
    m_config.user_interface.battery.colors.critical = c;
    return *this;
  }

  ApplicationConfigBuilder& set_battery_charging_color(ui::glfw::Color c) {
    m_config.user_interface.battery.colors.charging = c;
    return *this;
  }

  ApplicationConfigBuilder&
  set_font_config(ui::UserInterfaceConfig::FontConfig cfg) {
    m_config.user_interface.font = cfg;
    return *this;
  }

  ApplicationConfig build() const noexcept { return m_config; }
};
