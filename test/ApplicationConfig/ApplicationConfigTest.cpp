#include "ApplicationConfigBuilder.h"
#include "InvalidConfigTests.h"
#include "ValidConfigTests.h"
#include "ui/UserInterfaceConfig.h"

#include <ApplicationConfig.h>
#include <gtest/gtest.h>

namespace {
  bool same_colors(const ui::glfw::Color& c1,
                   const ui::glfw::Color& c2) noexcept {
    return c1.r == c2.r && c1.g == c2.g && c1.b == c2.b;
  }

  bool are_same(const ApplicationConfig& cfg1,
                const ApplicationConfig& cfg2) noexcept {
    const auto& ui1 = cfg1.user_interface;
    const auto& ui2 = cfg2.user_interface;
    return ui1.battery.critical == ui2.battery.critical &&
           same_colors(ui1.colors.background, ui1.colors.background) &&
           same_colors(ui1.colors.text, ui1.colors.text) &&
           same_colors(ui1.battery.colors.charging,
                       ui1.battery.colors.charging) &&
           same_colors(ui1.battery.colors.normal, ui1.battery.colors.normal) &&
           same_colors(ui1.battery.colors.full, ui1.battery.colors.full) &&
           same_colors(ui1.battery.colors.critical,
                       ui1.battery.colors.critical) &&
           ui1.font.path == ui2.font.path && ui1.font.size == ui2.font.size &&
           ui1.time.format == ui2.time.format &&
           ui1.uptime.format == ui2.uptime.format;
  }
} // namespace

class ValidConfigFile
    : public testing::TestWithParam<valid_config_tests::TestParam> {};

TEST_P(ValidConfigFile, ValidConfigProvided_DefaultValuesAppliedIfMissing) {
  auto [test_config_path, expected_application_config] = GetParam();
  auto parsed_config =
      ApplicationConfig::from(std::filesystem::path(test_config_path));
  EXPECT_TRUE(are_same(parsed_config, expected_application_config));
}

INSTANTIATE_TEST_SUITE_P(
    ValidConfigTests, ValidConfigFile, valid_config_tests::test_samples,
    [](const testing::TestParamInfo<ValidConfigFile::ParamType>& info)
        -> std::string {
      auto filepath = info.param.path;
      return filepath.stem();
    });

class InvalidConfigTest
    : public testing::TestWithParam<invalid_config_tests::TestParam> {};

TEST_P(InvalidConfigTest, InvalidConfigProvided_ScenarioVerified) {
  auto [test_config_path, expected_application_config, scenario] = GetParam();
  scenario(test_config_path, expected_application_config);
}

INSTANTIATE_TEST_SUITE_P(
    InvalidConfigTests, InvalidConfigTest, invalid_config_tests::test_samples,
    [](const testing::TestParamInfo<InvalidConfigTest::ParamType>& info)
        -> std::string {
      auto filepath = info.param.path;
      return filepath.stem();
    });
