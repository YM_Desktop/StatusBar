#pragma once
#include "ApplicationConfigBuilder.h"

#include <ApplicationConfig.h>
#include <exception>
#include <gtest/gtest.h>
#include <optional>

namespace invalid_config_tests {
  struct TestParam {
    std::filesystem::path path;
    std::optional<ApplicationConfig> config;
    std::function<void(const std::filesystem::path&,
                       std::optional<ApplicationConfig>)>
        scenario;
  };

  const auto test_samples = testing::Values(
      TestParam{
          .path = "data/invalid_uptime_format.yaml",
          .config = std::nullopt,
          .scenario = [](const std::filesystem::path& p,
                         std::optional<ApplicationConfig> c) noexcept -> void {
            EXPECT_EQ(std::nullopt, c);
            EXPECT_THROW(ApplicationConfig::from(std::filesystem::path(p)),
                         std::exception);
          }},
      TestParam{
          .path = "data/invalid_time_format.yaml",
          .config = ApplicationConfigBuilder{}
                        .set_time_config({.format = {"10"}})
                        .build(),
          .scenario = [](const std::filesystem::path& p,
                         std::optional<ApplicationConfig> c) noexcept -> void {
            EXPECT_NO_THROW(ApplicationConfig::from(std::filesystem::path(p)));
          }},
      TestParam{
          .path = "data/invalid_font_path.yaml",
          .config = ApplicationConfig{},
          .scenario = [](const std::filesystem::path& p,
                         std::optional<ApplicationConfig> c) noexcept -> void {
            EXPECT_NE(std::nullopt, c);
            ApplicationConfig app_config;
            auto default_font_path = app_config.user_interface.font.path;
            EXPECT_NO_THROW(
                app_config = ApplicationConfig::from(std::filesystem::path(p)));
            EXPECT_EQ(app_config.user_interface.font.path, default_font_path);
          }},
      TestParam{
          .path = "data/invalid_font_size.yaml",
          .config = std::nullopt,
          .scenario = [](const std::filesystem::path& p,
                         std::optional<ApplicationConfig> c) noexcept -> void {
            EXPECT_EQ(std::nullopt, c);
            EXPECT_THROW(ApplicationConfig::from(std::filesystem::path(p)),
                         std::exception);
          }},
      TestParam{
          .path = "data/invalid_background_color.yaml",
          .config = std::nullopt,
          .scenario = [](const std::filesystem::path& p,
                         std::optional<ApplicationConfig> c) noexcept -> void {
            EXPECT_EQ(std::nullopt, c);
            EXPECT_THROW(ApplicationConfig::from(std::filesystem::path(p)),
                         std::exception);
          }});
} // namespace invalid_config_tests
