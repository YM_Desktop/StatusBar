#include <ParsedArgs.h>
#include <array>
#include <gtest/gtest.h>
#include <stdexcept>

TEST(ParsedArgsTest, ParseNoConfigFileSpecified) {
  std::string arg{""};
  char* argv = arg.data();
  ParsedArgs pargs(1, &argv);
  const auto config_path = pargs.config_path;
  EXPECT_EQ(std::nullopt, config_path);
}

TEST(ParsedArgsTest, ParseExistingConfigFileSpecified) {
  std::array<std::string, 2> args{"test", "--config=data/existing_config.yaml"};
  char* argv[] = {args.at(0).data(), args.at(1).data()};
  ParsedArgs pargs(2, argv);
  const auto config_path = pargs.config_path;
  ASSERT_TRUE(config_path);
  EXPECT_EQ(std::filesystem::path("data/existing_config.yaml"), config_path);
}

TEST(ParsedArgsTest, ParseNotExistingConfigFileSpecified) {
  std::array<std::string, 2> args{"test",
                                  "--config=data/not_existing_config.yaml"};
  char* argv[] = {args.at(0).data(), args.at(1).data()};
  EXPECT_THROW(ParsedArgs pargs(2, argv), std::runtime_error);
}
