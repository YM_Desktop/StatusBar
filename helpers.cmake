function(get_build_type result)
	string(TOLOWER "${CMAKE_BUILD_TYPE}" btype)
	set(${result} ${btype} PARENT_SCOPE)
endfunction()

function(is_debug_build result)
	get_build_type(btype)

	if(${btype} STREQUAL "debug")
		set(${result} TRUE PARENT_SCOPE)
	else()
		set(${result} FALSE PARENT_SCOPE)
	endif()
	
endfunction()
