#pragma once

namespace info {
  /**
   * @class BatteryStatus
   * @brief Enum for battery charge status.
   */
  enum class BatteryStatus { NO_BATTERY, DISCHARGING, CHARGING, FULL };

  /**
   * @struct BatteryData
   * @brief Contains battery level data.
   */
  struct BatteryData {
    /**
     * @brief Battery level.
     */
    unsigned char level;

    /**
     * @brief Battery status.
     */
    BatteryStatus status;
  };
} // namespace info

