#pragma once

#include <chrono>

namespace info {
  /**
   * @struct TimeData
   * @brief Contains current time
   */
  struct TimeData {
    /**
     * @brief Holds last system time point.
     */
    std::tm timestamp;
  };
} // namespace info
