#pragma once
#include <vector>

namespace info {
  /**
   * @struct CpuLoadData
   * @brief Current cpu load percentage.
   */
  struct CpuLoadData {
    /**
     * @brief Load level in percents.
     */
    unsigned char level;

    /**
     * @brief Each cpu level.
     */
    std::vector<unsigned char> cpu_levels;
  };
} // namespace info

