#pragma once

#include <chrono>

namespace info {
  /**
   * @struct UptimeData
   * @brief Current user uptime.
   */
  struct UptimeData {
    /**
     * @brief System uptime in seconds.
     */
    std::chrono::seconds uptime;
  };
} // namespace info

