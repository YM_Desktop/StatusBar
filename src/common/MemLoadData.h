#pragma once

namespace info {
  /**
   * @struct MemLoadData
   * @brief Free memory amount.
   */
  struct MemLoadData {
    /**
     * @brief Free memory percentage.
     */
    unsigned char level;

    /**
     * @brief Free memory amount in megabytes.
     */
    unsigned int megabytes;
  };
} // namespace info

