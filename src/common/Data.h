#pragma once

#include "BatteryData.h"
#include "CpuLoadData.h"
#include "MemLoadData.h"
#include "TimeData.h"
#include "UptimeData.h"

namespace info {

  /**
   * @struct Data
   * @brief Represents data to be displayed on UI.
   */
  struct Data {
    /**
     * @brief Current time data.
     */
    TimeData time;

    /**
     * @brief Current battery data.
     */
    BatteryData battery;

    /**
     * @brief Current cpu load data.
     */
    CpuLoadData cpu;

    /**
     * @brief Current memory load data.
     */
    MemLoadData memory;

    /**
     * @brief Current uptime data.
     */
    UptimeData uptime;
  };
} // namespace info
