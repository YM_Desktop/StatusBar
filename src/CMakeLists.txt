set(COMMON_DEFINITIONS ${CMAKE_CURRENT_SOURCE_DIR}/common)

add_subdirectory(info)
add_subdirectory(ui)

add_library(app_config
	OBJECT
	${CMAKE_CURRENT_SOURCE_DIR}/ApplicationConfig.cpp
)
target_include_directories(app_config
	PUBLIC
	${YAML_INCLUDE_DIR}
)
add_dependencies(app_config
	yaml_cpp
)

add_library(app
	OBJECT
	${CMAKE_CURRENT_SOURCE_DIR}/Application.cpp
)
target_include_directories(app
	PUBLIC
	${CMAKE_CURRENT_SOURCE_DIR}/common
	${CMAKE_CURRENT_SOURCE_DIR}/ui
)
string(TOUPPER ${UI_TYPE} UI_TYPE_DEFINITION)
target_compile_definitions(app
	PUBLIC
	UI_TYPE_${UI_TYPE_DEFINITION}
)

add_library(parsed_args
	OBJECT
	${CMAKE_CURRENT_SOURCE_DIR}/ParsedArgs.cpp
)
target_include_directories(parsed_args
	PUBLIC
	${ARGPARSE_INCLUDE_DIR}
)
add_dependencies(parsed_args
	argparse_lib
)

add_executable(${PROJECT_NAME}
	${CMAKE_CURRENT_SOURCE_DIR}/main.cpp
)
target_link_libraries(${PROJECT_NAME}
	PRIVATE
	info
	ui
	app_config
	app
	parsed_args
	${YAML_SHARED_LIB}
)
target_include_directories(${PROJECT_NAME}
	PUBLIC
	${YAML_INCLUDE_DIR}
)
target_compile_options(${PROJECT_NAME}
	PUBLIC
	$<$<CONFIG:Debug>:-g>
	$<$<CONFIG:Release>:-O3>
	-fPIC 
	-Wall 
	-Wextra
	-Wpedantic
)
add_dependencies(${PROJECT_NAME} info ui yaml_cpp)

if(${BUILD_DOC})
	include(${DEPS_ROOT_SOURCE}/doc/doxygen.cmake)
	add_dependencies(${PROJECT_NAME} code_documentation)
endif()
