#include "ParsedArgs.h"

#include <argparse/argparse.hpp>
#include <filesystem>
#include <stdexcept>

namespace {
  constexpr std::string config_option_name{ "--config" };
}

ParsedArgs::ParsedArgs(int argc, char** argv) {
  argparse::ArgumentParser parser;
  parser.add_argument(config_option_name).default_value(std::string{});

  parser.parse_args(argc, argv);

  if (!parser.is_used(config_option_name)) {
    return;
  }

  auto parsed_config_path = parser.get<std::string>(config_option_name);
  if (std::filesystem::path config_filepath{ parsed_config_path };
      std::filesystem::status_known(std::filesystem::status(config_filepath)) &&
      std::filesystem::exists(config_filepath)) {
    config_path = config_filepath;
    return;
  }

  throw std::runtime_error("Config filepath provided is invalid!");
}

ParsedArgs::~ParsedArgs() = default;
