#include "ApplicationConfig.h"

#include "ui/UserInterfaceConfig.h"

#include <filesystem>
#include <stdexcept>
#include <type_traits>
#include <yaml-cpp/yaml.h>

namespace {
  template <typename T>
  void set_config_item_or_default(YAML::Node node, T& config_item) {
    if (node) {

      using UptimeFormat = ui::UserInterfaceConfig::Uptime::Format;
      using Color = ui::glfw::Color;
      using Path = std::filesystem::path;

      constexpr bool is_uptime_format = std::is_same_v<T, UptimeFormat>;
      constexpr bool is_filepath = std::is_same_v<T, Path>;
      constexpr bool is_color = std::is_same_v<T, Color>;

      if constexpr (is_uptime_format) {
        config_item = static_cast<UptimeFormat>(node.as<unsigned char>());
      } else if constexpr (is_filepath) {
        config_item = node.as<std::string>();
      } else if constexpr (is_color) {
        auto color_rgb_spec = node.as<std::vector<unsigned char>>();
        if (color_rgb_spec.size() != 3ul) {
          throw std::runtime_error("Invalid spec for RGB color! Only r, g, b "
                                   "components can be specified");
        }
        config_item = Color{ .r = color_rgb_spec.at(0),
                             .g = color_rgb_spec.at(1),
                             .b = color_rgb_spec.at(2) };
      } else {
        config_item = node.as<T>();
      }
    }
  }
} // namespace

ApplicationConfig ApplicationConfig::from(std::filesystem::path&& config_file) {
  ApplicationConfig config;
  auto yaml_config = YAML::LoadFile(config_file.string());

  auto user_interface = yaml_config["ui"];
  auto& ui_config = config.user_interface;

  auto font = user_interface["font"];
  auto& font_config = ui_config.font;
  auto default_font_path = font_config.path;
  set_config_item_or_default(font["path"], font_config.path);
  if (!std::filesystem::exists(font_config.path)) {
    font_config.path = default_font_path;
  }
  set_config_item_or_default(font["size"], font_config.size);

  auto battery = user_interface["battery"];
  auto& battery_config = ui_config.battery;
  set_config_item_or_default(battery["critical_level"],
                             battery_config.critical);

  auto battery_colors = battery["colors"];
  auto& battery_colors_config = battery_config.colors;

  set_config_item_or_default(battery_colors["normal"],
                             battery_colors_config.normal);
  set_config_item_or_default(battery_colors["full"],
                             battery_colors_config.full);
  set_config_item_or_default(battery_colors["charging"],
                             battery_colors_config.charging);
  set_config_item_or_default(battery_colors["critical"],
                             battery_colors_config.critical);

  auto uptime = user_interface["uptime"];
  auto& uptime_config = ui_config.uptime;
  set_config_item_or_default(uptime["format"], uptime_config.format);

  auto time = user_interface["time"];
  auto& time_config = ui_config.time;
  set_config_item_or_default(time["format"], time_config.format);

  auto colors = user_interface["colors"];
  auto& colors_config = ui_config.colors;
  set_config_item_or_default(colors["background"], colors_config.background);
  set_config_item_or_default(colors["text"], colors_config.text);

  return config;
}
