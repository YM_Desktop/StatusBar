#pragma once

#if defined(UI_TYPE_GLFW)
#include "glfw_impl/Window.h"

namespace ui {
  using UserInterface = glfw::Window;
}
#else
static_assert(false, "Undefined UI implementation!");
#endif
