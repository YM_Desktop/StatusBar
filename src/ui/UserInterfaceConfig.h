#pragma once

#include "glfw_impl/Color.h"

#include <filesystem>
#include <string>

namespace ui {
  /**
   * @struct UserInterfaceConfig
   * @brief Holds parsed user interface config.
   */
  struct UserInterfaceConfig {

    /**
     * @struct FontConfig
     * @brief Contains font configuration.
     */
    struct FontConfig {
      /**
       * @brief Path to font file.
       */
      std::filesystem::path path{"/usr/share/fonts/TTF/RobotoMono-Regular.ttf"};

      /**
       * @brief Size of font.
       */
      std::size_t size{30ul};
    }
    /**
     * @brief Font configuration instance
     */
    font;

    /**
     * @struct Colors
     * @brief Color pallete for application.
     */
    struct Colors {
      /**
       * @brief Window background color
       */
      glfw::Color background{0, 0, 0};

      /**
       * @brief Text color
       */
      glfw::Color text{127, 127, 127};
    }
    /**
     * @brief Colors configuration instance.
     */
    colors;

    /**
     * @struct Battery
     * @brief Battery indicator settings
     */
    struct Battery {
      /**
       * @brief Critical level of battery in percents.
       */
      unsigned char critical = 20u;

      /**
       * @struct Colors
       * @brief Battery indicator colors
       */
      struct Colors {
        /**
         * @brief Normal level color.
         */
        glfw::Color normal{0, 180, 0};
        /**
         * @brief Full charged level color.
         */
        glfw::Color full{0, 255, 255};
        /**
         * @brief Charging level color.
         */
        glfw::Color charging{0, 0, 190};
        /**
         * @brief Critical level color.
         */
        glfw::Color critical{190, 0, 0};
      }
      /**
       * @brief Battery colors configuration instance.
       */
      colors;
    }
    /**
     * @brief Battery indicator configuration instance.
     */
    battery;

    /**
     * @struct Uptime
     * @brief Uptime display data
     */
    struct Uptime {
      /**
       * @enum Format
       * @brief Uptime display format.
       */
      enum class Format {
        /**
         * @brief Display uptime in seconds only.
         */
        SECONDS,

        /**
         * @brief Display uptime in minutes only.
         */
        MINUTES,

        /**
         * @brief Display uptime in human-readable format.
         * E.g. 4h 20m.
         */
        HUMAN
      }
      /**
       * @brief Uptime display format instance.
       */
      format{Format::HUMAN};
    }
    /**
     * @brief Uptime config instance.
     */
    uptime;

    /**
     * @struct Time
     * @brief time data configuration.
     */
    struct Time {
      /**
       * @brief Time display format.
       */
      std::string format{"%A %c"};
    }
    /**
     * @brief Time config instance.
     */
    time;

    /**
     * @struct Cpu
     * @brief Cpu load configuration.
     */
    struct Cpu {
      /**
       * @brief Cpu load critical level.
       */
      unsigned char critical = 95u;

      /**
       * @struct Colors
       * @brief Cpu load colors.
       */
      struct Colors {
        /**
         * @brief Normal level color.
         */
        glfw::Color normal{0, 180, 0};
        /**
         * @brief Critical level color.
         */
        glfw::Color critical{0, 100, 100};
      }
      /**
       * @brief Cpu load colors.
       */
      colors;
    }
    /**
     * @brief CPU load rendering config.
     */
    cpu;
  };
} // namespace ui

