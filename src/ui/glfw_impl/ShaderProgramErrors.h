#pragma once
#include <stdexcept>

namespace ui::glfw {
  /**
   * @struct ProgramLinkError
   * @brief Thrown, when ShaderProgram has problems with linkage.
   */
  struct ProgramLinkError : public std::runtime_error {
    /**
     * @brief Constructor
     * @param err_msg
     */
    ProgramLinkError(const char* err_msg)
        : std::runtime_error(err_msg) {}
  };
} // namespace ui::glfw

