#include "Shader.h"

#include "GL_Headers.h"
#include "ShaderErrors.h"

#include <array>
#include <string>

namespace ui::glfw {

  Shader::Shader(std::string_view source, GLenum shader_type) {
    m_id = glCreateShader(shader_type);
    auto* source_data = source.data();
    glShaderSource(m_id, 1, &source_data, nullptr);
    glCompileShader(m_id);
    validate_compilation_status();
  }

  void Shader::validate_compilation_status() const {
    int err_code;
    glGetShaderiv(m_id, GL_COMPILE_STATUS, &err_code);

    if (err_code != GL_TRUE) {
      constexpr std::size_t err_msg_maxsize{ 1024ul };
      std::array<char, err_msg_maxsize> info;
      glGetShaderInfoLog(m_id, err_msg_maxsize, nullptr, info.data());
      throw ShaderCompilationError(info.data());
    }
  }

  Shader::~Shader() { glDeleteShader(m_id); }

  GLuint Shader::id() const noexcept { return m_id; }
} // namespace ui::glfw
