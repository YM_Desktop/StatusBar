#pragma once
#include <ft2build.h>
#include <string_view>
#include FT_FREETYPE_H

#include "FontCharInfo.h"

namespace ui::glfw {

  /**
   * @class FontLoader
   * Loads font with specified name and size
   */
  class FontLoader {
    /**
     * @brief FreeType lib handler
     */
    FT_Library m_lib;

    /**
     * @brief FreeType lib font face handler
     */
    FT_Face m_face;

    /**
     * @brief Collection of font specific data about chars
     */
    FontCharsInfo m_chars;

    /**
     * @fn verify
     * @brief verifies if Font loading step is ok.
     * @param statement
     * @param err_msg: error message to supply
     * @throws FontLoaderError
     */
    void verify(bool statement, std::string_view err_msg) const;

    /**
     * @fn load_char
     * @param c: char to load
     * @return FontCharInfo info about char specific to font
     */
    FontCharInfo load_char(unsigned char c) const;

  public:
    /**
     * @brief Constructor
     */
    FontLoader(std::string_view font_name, std::size_t font_size);

    /**
     * @brief Destructor
     */
    ~FontLoader();

    /**
     * @return reference to char font data
     */
    const FontCharsInfo& chars_info() const& noexcept;

    /**
     * @return chars font data
     */
    FontCharsInfo chars_info() && noexcept;
  };
} // namespace ui::glfw
