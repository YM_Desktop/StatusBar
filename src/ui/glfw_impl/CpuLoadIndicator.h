#pragma once
#include "GPolygon.h"
#include "GRectangle.h"

#include <UserInterfaceConfig.h>
#include <vector>

namespace ui::glfw {

  class ShaderProgram;

  /**
   * @class CpuLoadIndicator
   * @brief Indicates cpu load per each thread/core.
   */
  class CpuLoadIndicator {

  public:
    /**
     * @brief Constructor
     * @param shader_program
     * @param placement
     * @param config
     */
    CpuLoadIndicator(const ShaderProgram& shader_program, Rectangle&& placement,
                     const UserInterfaceConfig::Cpu& config);

    /**
     * @brief Destructor
     */
    ~CpuLoadIndicator();

    /**
     * @fn render
     * @param average_cpu_level: average cpu level to render.
     * @param cpu_levels: each cpu level to render.
     * @param outline: rectangle, where all cpu levels must be placed.
     */
    void render(unsigned char average_cpu_level,
                std::vector<unsigned char> cpu_levels,
                Rectangle&& outline) const noexcept;

    /**
     * @class CpuCoreLoadIndicator
     * @brief Renders individual info about cpu core load.
     */
    class CpuCoreLoadIndicator {
    public:
      /**
       * @brief Constructor
       * @param shader_program
       * @param placement
       * @param config
       */
      CpuCoreLoadIndicator(const ShaderProgram& shader_program,
                           Rectangle&& placement,
                           const ::ui::UserInterfaceConfig::Cpu& config);

      /**
       * @brief Destructor
       */
      ~CpuCoreLoadIndicator();

      /**
       * @fn render
       * @param percentage: cpu level to render.
       * @param outline: rectangle to render.
       * @param margin: margin between level and outline.
       */
      void render(unsigned char percentage, Rectangle&& outline,
                  int margin) const noexcept;

    private:
      /**
       * @brief Outline rectangle.
       */
      GPolygon<4> m_outline;

      /**
       * @brief Fill rectangle, indicates load level.
       */
      GRectangle m_level;

      /**
       * @brief Color scheme and render params.
       */
      const ::ui::UserInterfaceConfig::Cpu& m_config;
    };

    /**
     * @brief Indicators per core.
     */
    std::vector<CpuCoreLoadIndicator> m_core_indicators;

    /**
     * @brief Number of cpus available
     */
    const unsigned int m_number_of_cpus;
  };
} // namespace ui::glfw
