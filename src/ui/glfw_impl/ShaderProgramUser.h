#pragma once
#include "ShaderProgram.h"

namespace ui::glfw {
  /**
   * @class ShaderProgramUser
   * @brief RAII wrapper for ShaderProgram
   */
  class ShaderProgramUser {
    /**
     * @brief program ref
     */
    const ShaderProgram& m_program;

  public:
    /**
     * @brief Constructor, enables shader program
     * @param p: program ref
     */
    ShaderProgramUser(const ShaderProgram& p)
        : m_program(p) {
      m_program.use();
    }

    /**
     * @brief Destructor, disables shader program
     */
    ~ShaderProgramUser() { m_program.disable(); }
  };
} // namespace ui::glfw
