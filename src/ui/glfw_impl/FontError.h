#pragma once

#include <stdexcept>

/**
 * @struct FontLoaderError
 * @brief Occurs when FontLoader fails
 */
struct FontLoaderError : public std::runtime_error {

  /**
   * @brief Constructor
   * @param err_msg
   */
  FontLoaderError(const char* err_msg)
      : std::runtime_error(err_msg) {}
};
