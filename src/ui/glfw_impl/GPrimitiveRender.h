#pragma once
#include "Color.h"
#include "GL_Headers.h"
#include "GPrimitiveRender.h"
#include "Point.h"
#include "ShaderProgramUser.h"

#include <array>

namespace ui::glfw {

  class ShaderProgram;
  class Color;

  /**
   * @class GPrimitiveRender
   * @brief Draws primitives and manages OpenGL handlers.
   * @tparam NumberOfPoints
   * @tparam NumberOfIndices
   */
  template <std::size_t NumberOfPoints, std::size_t NumberOfIndices>
  class GPrimitiveRender {

  public:
    /**
     * @brief Alias for base class.
     */
    using Base = GPrimitiveRender<NumberOfPoints, NumberOfIndices>;

    /**
     * @brief Alias for polygon points type.
     */
    using Points = std::array<Point, NumberOfPoints>;

    /**
     * @brief Alias for points indices.
     */
    using Indices = std::array<unsigned int, NumberOfIndices>;

    /**
     * @brief Alias for vertices updater type.
     */
    using VerticesUpdater = std::function<void(Points&)>;

  public:
    /**
     * @brief Constructor
     */
    GPrimitiveRender(
        const ShaderProgram& shader_program, GLenum primitive_type,
        const Points& points,
        const Indices& indices = []() -> Indices {
          static_assert(NumberOfPoints <= NumberOfIndices,
                        "Indices array overlaps points array");
          Indices inds;
          for (unsigned int v = 0u; auto& i : inds) {
            i = v++;
          }
          return inds;
        }())
        : m_vertex_array_id(0u)
        , m_vertex_buffer_id(0u)
        , m_shader_program(shader_program)
        , m_primitive_type(primitive_type)
        , m_points(points) {

      glGenVertexArrays(1, &m_vertex_array_id);
      glGenBuffers(1, &m_vertex_buffer_id);
      glGenBuffers(1, &m_elements_buffer_id);

      glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_elements_buffer_id);
      glBufferData(GL_ELEMENT_ARRAY_BUFFER,
                   indices.size() * sizeof(unsigned int), indices.data(),
                   GL_STATIC_DRAW);

      update_vertices();

      glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float),
                            nullptr);
      glEnableVertexAttribArray(0);

      glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
      glBindBuffer(GL_ARRAY_BUFFER, 0);
      glBindVertexArray(0);
    }

    /**
     * @brief Destructor
     */
    ~GPrimitiveRender() {
      glDisableVertexAttribArray(0);
      glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
      glBindBuffer(GL_ARRAY_BUFFER, 0);
      glBindVertexArray(0);
      glDeleteVertexArrays(1, &m_vertex_array_id);
      glDeleteBuffers(1, &m_elements_buffer_id);
      glDeleteBuffers(1, &m_vertex_buffer_id);
    }

    /**
     * @brief Updates color of primitive
     */
    void update_color(const Color& color) const noexcept {
      auto color_set =
          glGetUniformLocation(m_shader_program.id(), "figure_color_set");
      auto [r, g, b] = color;
      glUniform3f(color_set, r, g, b);
    }

    /**
     * @brief renders triangle
     */
    void render(const Color& color) const noexcept {
      ShaderProgramUser user(m_shader_program);

      update_color(color);

      glBindVertexArray(m_vertex_array_id);
      glBindBuffer(GL_ARRAY_BUFFER, m_vertex_buffer_id);
      glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_elements_buffer_id);
      glDrawElements(m_primitive_type, NumberOfIndices, GL_UNSIGNED_INT, 0);
    }

    /**
     * @fn update_vertices
     * @param update_points: points updater
     */
    void update_vertices(VerticesUpdater update_points = [](Points&) {
    }) const noexcept {

      update_points(m_points);

      glBindVertexArray(m_vertex_array_id);
      glBindBuffer(GL_ARRAY_BUFFER, m_vertex_buffer_id);

      std::array<float, 3 * NumberOfPoints> vertices;
      for (std::size_t vertices_index = 0ul; const auto& p : m_points) {
        auto [x, y, z] = p;
        vertices.at(vertices_index++) = x;
        vertices.at(vertices_index++) = y;
        vertices.at(vertices_index++) = z;
      }

      glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(float),
                   vertices.data(), GL_STATIC_DRAW);
    }

    /**
     * @fn points
     * @return Primitive points
     */
    const Points& points() const noexcept { return m_points; }

  private:
    /**
     * @brief vertex array id
     */
    unsigned int m_vertex_array_id;

    /**
     * @brief vertex buffer id
     */
    unsigned int m_vertex_buffer_id;

    /**
     * @brief elements buffer id
     */
    unsigned int m_elements_buffer_id;

    /**
     * @brief shader program, renders triangle
     */
    const ShaderProgram& m_shader_program;

    /**
     * @brief type of figure to draw
     */
    GLenum m_primitive_type;

    /**
     * @brief points of polygon.
     */
    mutable Points m_points;
  };

} // namespace ui::glfw
