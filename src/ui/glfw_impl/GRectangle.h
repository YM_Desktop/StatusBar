#pragma once

#include "Color.h"
#include "GFilledPolygon.h"
#include "Rectangle.h"

namespace ui::glfw {

  class ShaderProgram;
  class Color;

  /**
   * @class GRectangle
   * @brief Draws rectangle and manages OpenGL handlers.
   */
  class GRectangle : private GFilledPolygon<4ul, 6ul> {
    using Base = GFilledPolygon<4ul, 6ul>;

  public:
    /**
     * @brief Functor type alias for vertices updater.
     */
    using VerticesUpdater = Base::VerticesUpdater;

  public:
    /**
     * @brief Constructor
     */
    GRectangle(const ShaderProgram& shader_program,
               const std::array<Point, 2ul>& points);

    /**
     * @brief Destructor
     */
    ~GRectangle();

    using Base::points;
    using Base::render;
    using Base::update_vertices;
  };

} // namespace ui::glfw

