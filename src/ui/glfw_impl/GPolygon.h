#pragma once

#include "Color.h"
#include "GPrimitiveRender.h"
#include "Point.h"

namespace ui::glfw {

  class ShaderProgram;
  class Color;

  /**
   * @class GPolygon
   * @brief Draws polygon and manages OpenGL handlers.
   * @tparam NumberOfPoints
   * @tparam NumberOfIndices
   */
  template <std::size_t NumberOfPoints,
            std::size_t NumberOfIndices = NumberOfPoints>
  class GPolygon : private GPrimitiveRender<NumberOfPoints, NumberOfIndices> {
    using Base = GPrimitiveRender<NumberOfPoints, NumberOfIndices>;

  public:
    /**
     * @brief Points type alias
     */
    using Points = typename Base::Points;

  public:
    /**
     * @brief Constructor
     */
    GPolygon(const ShaderProgram& shader_program, const Points& points)
        : Base(shader_program, GL_LINE_LOOP, points) {}

    /**
     * @brief Destructor
     */
    ~GPolygon() = default;

    using Base::points;
    using Base::render;
    using Base::update_vertices;
  };

} // namespace ui::glfw
