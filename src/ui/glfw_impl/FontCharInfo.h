#pragma once
#include <unordered_map>

namespace ui::glfw {

  /**
   * @struct FontCharInfo
   * @brief Contains font specific info about char
   */
  struct FontCharInfo {
    /**
     * @brief texture id
     */
    unsigned int texture_id;

    /**
     * @brief char bitmap width
     */
    unsigned int bitmap_width;

    /**
     * @brief char bitmap height
     */
    unsigned int bitmap_rows;

    /**
     * @brief char bitmap left position
     */
    int bitmap_left;

    /**
     * @brief char bitmap top position
     */
    int bitmap_top;

    /**
     * @brief char advance
     */
    unsigned int advance;

    /**
     * @brief buffer pointer
     */
    unsigned char* buffer;
  };

  using FontCharsInfo = std::unordered_map<unsigned char, FontCharInfo>;
} // namespace ui::glfw
