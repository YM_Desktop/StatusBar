#pragma once
#include "Point.h"

namespace ui::glfw {

  /**
   * @struct Rectangle
   * @brief Represents rectangle.
   */
  struct Rectangle {
    /**
     * brief start point
     */
    Point left_bottom;

    /**
     * brief width
     */
    int width;

    /**
     * brief height
     */
    int height;
  };
} // namespace ui::glfw

