#include "Window.h"

#include "BatteryIndicator.h"
#include "Color.h"
#include "CpuLoadIndicator.h"
#include "EnvironmentErrors.h"
#include "FontCharInfo.h"
#include "FontError.h"
#include "FontLoader.h"
#include "GTextRender.h"
#include "Point.h"
#include "Shader.h"
#include "ShaderErrors.h"
#include "ShaderProgram.h"
#include "ShaderProgramErrors.h"
#include "ShaderProgramUser.h"
#include "ShaderSource.h"

#include <UserInterfaceConfig.h>
#include <array>
#include <ctime>

namespace {
  void key_pressed_handler(GLFWwindow* window,
                           int key,
                           int scancode,
                           int action,
                           int mods) noexcept {
    switch (key) {
    case GLFW_KEY_ESCAPE:
      glfwSetWindowShouldClose(window, 1);
      break;
    }
  }

  void framebuffer_size_callback(GLFWwindow* window, int width, int height) {
    // make sure the viewport matches the new window dimensions; note that width
    // and height will be significantly larger than specified on retina
    // displays.
    glViewport(0, 0, width, height);
  }

  void window_resize_callback(GLFWwindow* window, int width, int height) {
    ui::glfw::Window::width = width;
    ui::glfw::Window::height = height;
  }

  void error_handler(int err_code, const char* description) noexcept {
    std::printf("GLFW Error: %d -> %s", err_code, description);
  }

} // namespace

namespace ui::glfw {

  int Window::width;
  int Window::height;

  void Window::init() {
    if (GLFW_TRUE != glfwInit())
      throw EnvironmentError("Error initialising GLFW environment");

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    auto* monitor = glfwGetPrimaryMonitor();
    auto* video_mode = glfwGetVideoMode(monitor);

    m_window = glfwCreateWindow(
        video_mode->width, video_mode->height, "Some title", nullptr, nullptr);
    glfwGetWindowSize(m_window, &Window::width, &Window::height);
    if (!m_window)
      throw EnvironmentError("Error creating GLFW Window");

    glfwMakeContextCurrent(m_window);

    if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
      throw EnvironmentError("Error initialising GLAD");

    glfwSetKeyCallback(m_window, &key_pressed_handler);
    glfwSetFramebufferSizeCallback(m_window, &framebuffer_size_callback);
    glfwSetWindowSizeCallback(m_window, &window_resize_callback);
    glfwSetErrorCallback(&error_handler);

    // interval for refreshing window and checking events in seconds
    glfwSwapInterval(1);

    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
  }

  std::array<ShaderProgram, 2ul> Window::load_shaders() const {
    ShaderProgram simple_program(
        { ui::glfw::Shader(ui::glfw::simple_vertex_source, GL_VERTEX_SHADER),
          ui::glfw::Shader(ui::glfw::simple_fragment_source,
                           GL_FRAGMENT_SHADER) });

    ShaderProgram text_shader_program(
        { ui::glfw::Shader(ui::glfw::vertex_source, GL_VERTEX_SHADER),
          ui::glfw::Shader(ui::glfw::fragment_source, GL_FRAGMENT_SHADER) });

    return { simple_program, text_shader_program };
  }

  FontCharsInfo
  Window::load_chars_font_info(const std::filesystem::path& font_filepath,
                               std::size_t font_size) const {
    FontCharsInfo chars_info =
        FontLoader(font_filepath.c_str(), font_size).chars_info();
    return chars_info;
  }

  void Window::run(const ShaderProgram& simple_program,
                   const ShaderProgram& text_shader_program,
                   FontCharsInfo&& chars_info) {

    const int max_height = chars_info.at('X').bitmap_rows;

    for (const auto& shader_program : { simple_program, text_shader_program }) {
      ShaderProgramUser user(shader_program);
      auto width_uniform =
          glGetUniformLocation(shader_program.id(), "window_width");
      auto height_uniform =
          glGetUniformLocation(shader_program.id(), "window_height");
      glUniform1i(width_uniform, width);
      glUniform1i(height_uniform, height);
    }

    GTextRender tr(std::move(chars_info), text_shader_program);

    glfwGetWindowSize(m_window, &Window::width, &Window::height);

    Point start_point = Point{ .x = 5, .y = height / 2 - max_height - 10 };

    BatteryIndicator bi(simple_program,
                        Rectangle{ .left_bottom = start_point,
                                   .width = 50,
                                   .height = max_height },
                        m_cfg.battery.colors,
                        m_cfg.battery);

    CpuLoadIndicator ci(
        simple_program,
        {
            .left_bottom = {10, 10},
              .width = 50, .height = max_height
    },
        m_cfg.cpu);

    constexpr int margin_between_info = 30;
    const auto [br, bg, bb] = m_cfg.colors.background;

    while (!glfwWindowShouldClose(m_window)) {
      glClear(GL_COLOR_BUFFER_BIT);
      glClearColor(br / 255.0f, bg / 255.0f, bb / 255.0f, 1);

      {
        std::unique_lock lk(m_data_mutex);
        int x_shift = 0;
        x_shift = render_battery_level(Point(start_point), tr);
        x_shift = render_battery_indicator({ .x = x_shift + margin_between_info,
                                             .y = start_point.y,
                                             .z = start_point.z },
                                           bi,
                                           tr,
                                           max_height);
        x_shift = render_time({ .x = x_shift + margin_between_info,
                                .y = start_point.y,
                                .z = start_point.z },
                              tr);
        x_shift = render_uptime({ .x = x_shift + margin_between_info,
                                  .y = start_point.y,
                                  .z = start_point.z },
                                tr);
        x_shift = render_cpu_load({ .x = x_shift + margin_between_info,
                                    .y = start_point.y,
                                    .z = start_point.z },
                                  tr,
                                  ci);
        x_shift = render_mem_load({ .x = x_shift + margin_between_info,
                                    .y = start_point.y,
                                    .z = start_point.z },
                                  tr);
      }
      glfwSwapBuffers(m_window);
      glfwPollEvents();
    }
  }

  int Window::render_battery_level(Point&& start_point,
                                   GTextRender& tr) noexcept {
    auto [width, height] =
        tr.render(m_data.battery.status == info::BatteryStatus::NO_BATTERY
                      ? "PLUGGED!"
                      : std::to_string(m_data.battery.level) + "%",
                  Point(start_point),
                  Color(m_cfg.colors.text));

    return start_point.x + width;
  }

  int Window::render_battery_indicator(Point&& start_point,
                                       BatteryIndicator& bi,
                                       GTextRender& tr,
                                       int height) noexcept {
    if (m_data.battery.status == info::BatteryStatus::NO_BATTERY) {
      return 0;
    }

    constexpr int width = 50;

    bi.render(m_data.battery.level,
              Rectangle{ .left_bottom = start_point,
                         .width = width,
                         .height = height },
              height / 4);

    return start_point.x + width;
  }

  int Window::render_time(Point&& start_point, GTextRender& tr) noexcept {
    std::array<char, 100> time_raw_string;
    auto actually_written_bytes = std::strftime(time_raw_string.data(),
                                                time_raw_string.size(),
                                                m_cfg.time.format.c_str(),
                                                &m_data.time.timestamp);
    auto [width, height] =
        tr.render({ time_raw_string.data(),
                    time_raw_string.data() + actually_written_bytes },
                  Point(start_point),
                  m_cfg.colors.text);

    return start_point.x + width;
  }

  int Window::render_uptime(Point&& start_point, GTextRender& tr) noexcept {
    using namespace std::chrono;

    std::string formatted_uptime;
    if (m_cfg.uptime.format == UserInterfaceConfig::Uptime::Format::SECONDS) {
      formatted_uptime = std::to_string(m_data.uptime.uptime.count()) + "s";
    }

    if (m_cfg.uptime.format == UserInterfaceConfig::Uptime::Format::MINUTES) {
      auto mins = duration_cast<minutes>(m_data.uptime.uptime).count();
      formatted_uptime = std::to_string(mins) + "m";
    }

    if (m_cfg.uptime.format == UserInterfaceConfig::Uptime::Format::HUMAN) {
      auto day = duration_cast<days>(m_data.uptime.uptime).count();
      auto hour = duration_cast<hours>(m_data.uptime.uptime).count();
      auto min = duration_cast<minutes>(m_data.uptime.uptime).count();
      auto sec = m_data.uptime.uptime.count();
      formatted_uptime += day ? std::to_string(day) + "d " : "";
      formatted_uptime += hour ? std::to_string(hour - (day * 24)) + "h " : "";
      formatted_uptime += min ? std::to_string(min - (hour * 60)) + "m " : "";
      formatted_uptime += std::to_string(sec - (min * 60)) + "s";
    }

    auto [width, height] = tr.render(
        std::move(formatted_uptime), Point(start_point), m_cfg.colors.text);
    return start_point.x + width;
  }

  int Window::render_cpu_load(Point&& start_point,
                              GTextRender& tr,
                              CpuLoadIndicator& ci) noexcept {

    auto [width, height] = tr.render(std::to_string(m_data.cpu.level) + "%",
                                     Point(start_point),
                                     m_cfg.colors.text);

    ci.render(m_data.cpu.level,
              m_data.cpu.cpu_levels,
              {
                  .left_bottom = {.x = start_point.x + width + 10,
                                  .y = start_point.y},
                  .width = 150,
                  .height = height
    });

    return start_point.x + width + 10 + 150;
  }

  int Window::render_mem_load(Point&& start_point, GTextRender& tr) noexcept {
    auto [width, height] =
        tr.render(std::to_string(m_data.memory.level) + "% " +
                      std::to_string(m_data.memory.megabytes) + "MB",
                  Point(start_point),
                  m_cfg.colors.text);

    return start_point.x + width;
  }

  Window::Window(const UserInterfaceConfig& cfg)
      : m_cfg(cfg)
      , m_runner(std::async(std::launch::async,
                            &Window::window_thread_runner,
                            this)) {}

  void Window::window_thread_runner() try {
    init();

    auto [simple_program, text_shader_program] = load_shaders();

    auto chars_info = load_chars_font_info(m_cfg.font.path, m_cfg.font.size);

    run(simple_program, text_shader_program, std::move(chars_info));
  } catch (const ui::glfw::EnvironmentError& err) {
    std::printf(err.what());
  } catch (const ui::glfw::ShaderCompilationError& err) {
    std::printf(err.what());
  } catch (const ui::glfw::ProgramLinkError& err) {
    std::printf(err.what());
  } catch (const FontLoaderError& err) {
    std::printf(err.what());
  }

  bool Window::update(info::Data data) noexcept {
    std::unique_lock lk(m_data_mutex);
    m_data = data;
    return std::future_status::ready !=
           m_runner.wait_for(std::chrono::milliseconds(1u));
  }

  Window::~Window() {
    if (m_window)
      glfwDestroyWindow(m_window);
    glfwTerminate();
  }
} // namespace ui::glfw
