#pragma once
#include <stdexcept>

namespace ui::glfw {
  /**
   * @struct ShaderCompilationError
   * @brief Thrown, when Shader has problems with compilation.
   */
  struct ShaderCompilationError : public std::runtime_error {
    /**
     * @brief Constructor
     * @param err_msg
     */
    ShaderCompilationError(const char* err_msg)
        : std::runtime_error(err_msg) {}
  };
} // namespace ui::glfw
