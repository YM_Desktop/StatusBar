#include "CpuLoadIndicator.h"

#include "glfw_impl/Point.h"
#include "glfw_impl/Rectangle.h"

#include <thread>

namespace {
  std::array<ui::glfw::Point, 2ul>
  level_points(unsigned char percentage,
               const ui::glfw::Rectangle& placement,
               int margin) noexcept {
    const auto& [left_bottom, width, height] = placement;
    std::array<ui::glfw::Point, 2ul> points = {
      ui::glfw::Point{        .x = left_bottom.x + margin,
                      .y = left_bottom.y + margin         },
      ui::glfw::Point{.x = left_bottom.x + width - margin,
                      .y = left_bottom.y + height - margin}
    };
    points.at(1).y =
        points.at(0).y + ((points.at(1).y - points.at(0).y) * percentage / 100);
    return points;
  }

  std::array<ui::glfw::Point, 4ul>
  outline_points(const ui::glfw::Rectangle& placement) noexcept {
    const auto& [left_bottom, width, height] = placement;
    std::array<ui::glfw::Point, 4ul> points;
    points.at(0) = left_bottom;
    points.at(1) =
        ui::glfw::Point{ .x = left_bottom.x, .y = left_bottom.y + height };
    points.at(2) = ui::glfw::Point{ .x = left_bottom.x + width,
                                    .y = left_bottom.y + height };
    points.at(3) =
        ui::glfw::Point{ .x = left_bottom.x + width, .y = left_bottom.y };
    return points;
  }

  std::array<ui::glfw::Point, 4ul> rectangle_to_polygon_points(
      const std::array<ui::glfw::Point, 2ul>& rp) noexcept {
    std::array<ui::glfw::Point, 4ul> points;
    points.at(0) = rp.at(0);
    points.at(1) = ui::glfw::Point{ rp.at(0).x, rp.at(1).y };
    points.at(2) = rp.at(1);
    points.at(3) = ui::glfw::Point{ rp.at(1).x, rp.at(0).y };
    return points;
  }
} // namespace

namespace ui::glfw {
  CpuLoadIndicator::CpuCoreLoadIndicator::CpuCoreLoadIndicator(
      const ShaderProgram& shader_program,
      Rectangle&& placement,
      const ::ui::UserInterfaceConfig::Cpu& config)
      : m_config(config)
      , m_level(shader_program, level_points(0, placement, 2))
      , m_outline(shader_program, outline_points(placement)) {}

  CpuLoadIndicator::CpuCoreLoadIndicator::~CpuCoreLoadIndicator() = default;

  void
  CpuLoadIndicator::CpuCoreLoadIndicator::render(unsigned char percentage,
                                                 Rectangle&& outline,
                                                 int margin) const noexcept {

    auto update_level_points =
        [percentage, &outline, margin = 2](
            std::array<Point, 4ul>& points) noexcept -> void {
      const auto& [left_bottom, width, height] = outline;
      auto level_left_bottom = ui::glfw::Point{ .x = left_bottom.x + margin,
                                                .y = left_bottom.y + margin };
      auto level_right_top =
          ui::glfw::Point{ .x = left_bottom.x + width - margin,
                           .y = left_bottom.y + height - margin };
      level_right_top.y =
          level_left_bottom.y +
          ((level_right_top.y - left_bottom.y) * percentage / 100);

      points.at(0) = level_left_bottom;
      points.at(1) = ui::glfw::Point{ level_left_bottom.x, level_right_top.y };
      points.at(2) = level_right_top;
      points.at(3) = ui::glfw::Point{ level_right_top.x, level_left_bottom.y };
    };

    m_level.update_vertices(update_level_points);
    m_level.render(percentage > m_config.critical ? m_config.colors.critical
                                                  : m_config.colors.normal);

    auto update_outline_points =
        [&outline](std::array<Point, 4ul>& points) noexcept -> void {
      const auto& [left_bottom, width, height] = outline;
      points.at(0) = left_bottom;
      points.at(1) =
          ui::glfw::Point{ .x = left_bottom.x, .y = left_bottom.y + height };
      points.at(2) = ui::glfw::Point{ .x = left_bottom.x + width,
                                      .y = left_bottom.y + height };
      points.at(3) =
          ui::glfw::Point{ .x = left_bottom.x + width, .y = left_bottom.y };
    };
    m_outline.update_vertices(update_outline_points);
    m_outline.render(m_config.colors.normal);
  }
} // namespace ui::glfw

namespace ui::glfw {
  CpuLoadIndicator::CpuLoadIndicator(
      const ShaderProgram& shader_program,
      Rectangle&& placement,
      const ::ui::UserInterfaceConfig::Cpu& config)
      : m_number_of_cpus(std::thread::hardware_concurrency()) {
    const auto width_of_cpu_load_segment = placement.width / m_number_of_cpus;
    auto start_point = placement.left_bottom;

    m_core_indicators.reserve(m_number_of_cpus);
    for (auto cpu = 0; cpu < m_number_of_cpus; ++cpu) {
      m_core_indicators.emplace_back(
          shader_program,
          Rectangle{ .left_bottom = start_point,
                     .width = static_cast<int>(width_of_cpu_load_segment),
                     .height = placement.height },
          config);
    }
  }

  CpuLoadIndicator::~CpuLoadIndicator() = default;

  void CpuLoadIndicator::render(unsigned char average_cpu_level,
                                std::vector<unsigned char> cpu_levels,
                                Rectangle&& outline) const noexcept {
    if (m_core_indicators.size() != cpu_levels.size()) {
      return;
    }

    const auto width_of_cpu_load_segment = outline.width / m_number_of_cpus;
    auto cores = cpu_levels.size();
    auto start_point = outline.left_bottom;
    for (auto core = 0; core < cores; ++core) {
      m_core_indicators.at(core).render(
          cpu_levels.at(core),
          { .left_bottom = start_point,
            .width = static_cast<int>(width_of_cpu_load_segment),
            .height = outline.height },
          2);
      start_point.x += width_of_cpu_load_segment;
    }
  }
} // namespace ui::glfw
