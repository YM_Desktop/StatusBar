#include "ShaderProgram.h"

#include "GL_Headers.h"
#include "ShaderProgramErrors.h"

#include <array>
#include <stdexcept>

namespace ui::glfw {
  ShaderProgram::ShaderProgram(Shaders&& shaders)
      : m_id(glCreateProgram()) {
    for (const auto& shader : shaders) {
      glAttachShader(m_id, shader.id());
    }
    glLinkProgram(m_id);
    validate_link_status();
  }

  void ShaderProgram::validate_link_status() const {
    int err_code;
    glGetProgramiv(m_id, GL_LINK_STATUS, &err_code);

    if (err_code != GL_TRUE) {
      constexpr std::size_t err_msg_maxsize{ 1024ul };
      std::array<char, err_msg_maxsize> info;
      glGetProgramInfoLog(m_id, err_msg_maxsize, nullptr, info.data());
      throw ProgramLinkError(info.data());
    }
  }

  ShaderProgram::~ShaderProgram() { disable(); }

  void ShaderProgram::use() const noexcept { glUseProgram(m_id); }

  void ShaderProgram::disable() const noexcept { glUseProgram(0); }

  GLuint ShaderProgram::id() const noexcept { return m_id; }
} // namespace ui::glfw
