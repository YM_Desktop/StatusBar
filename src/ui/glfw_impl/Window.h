#pragma once
#include "Data.h"
#include "FontCharInfo.h"
#include "ShaderProgram.h"

#include <array>
#include <filesystem>
#include <future>
#include <mutex>

extern "C" {
struct GLFWwindow;
}

namespace ui {
  struct UserInterfaceConfig;
}

namespace ui::glfw {

  struct Point;
  class BatteryIndicator;
  class CpuLoadIndicator;
  class GTextRender;

  /**
   * @class Window
   * @brief Holds Window handlers
   */
  class Window {

    /**
     * @brief glfw specific window handler
     */
    GLFWwindow* m_window;

    /**
     * @brief Data to display
     */
    info::Data m_data;

    /**
     * @brief Mutex for data access.
     */
    std::mutex m_data_mutex;

    /**
     * @brief Window thread handler.
     */
    std::future<void> m_runner;

    /**
     * @brief Reference to user configuration.
     */
    const ::ui::UserInterfaceConfig& m_cfg;

    /**
     * @fn init
     * @brief initialises window and gl environment
     * @throws EnvironmentError
     */
    void init();

    /**
     * @fn load_shaders
     * @brief loads shaders and produces shader programs.
     * @return array of shader programs
     */
    std::array<ShaderProgram, 2ul> load_shaders() const;

    /**
     * @fn load_chars_font_info
     * @brief Loads font glyhs for chars and returns info about each char.
     * @param font_filepath: path to the ttf font
     * @param font_size: pixel height of selected font
     * @return FontCharsInfo
     */
    FontCharsInfo
    load_chars_font_info(const std::filesystem::path& font_filepath,
                         std::size_t font_size) const;

    /**
     * @fn run
     * @brief Runs window event loop
     * @param simple_program: shader program for primitives rendering
     * @param text_shader_program: shader program for text rendering
     * @param chars_info: font specific glyphs info about each char
     */
    void run(const ShaderProgram& simple_program,
             const ShaderProgram& text_shader_program,
             FontCharsInfo&& chars_info);

    /**
     * @fn window_thread_runner
     * @brief represents window thread
     * Runs init and run methods of current window.
     * Specific for OpenGL implementation (handlers and calls must be in one
     * tread!)
     */
    void window_thread_runner();

    /**
     * @fn render_battery_level
     * @param start_point: point, where to start rendering
     * @return last right X position.
     */
    int render_battery_level(Point&& start_point, GTextRender& tr) noexcept;

    /**
     * @fn render_battery_indicator
     * @param start_point: point, where to start rendering
     * @return last right X position.
     */
    int render_battery_indicator(Point&& start_point, BatteryIndicator& bi,
                                 GTextRender& tr, int height) noexcept;

    /**
     * @fn render_time
     * @param start_point: point, where to start rendering
     * @return last right X position.
     */
    int render_time(Point&& start_point, GTextRender& tr) noexcept;

    /**
     * @fn render_uptime
     * @param start_point: point, where to start rendering
     * @return last right X position.
     */
    int render_uptime(Point&& start_point, GTextRender& tr) noexcept;
    /**
     * @fn render_cpu_load
     * @param start_point: point, where to start rendering
     * @return last right X position.
     */
    int render_cpu_load(Point&& start_point, GTextRender& tr,
                        CpuLoadIndicator& ci) noexcept;

    /**
     * @fn render_mem_load
     * @param start_point: point, where to start rendering
     * @return last right X position.
     */
    int render_mem_load(Point&& start_point, GTextRender& tr) noexcept;

  public:
    /**
     * @brief Constructor
     */
    Window(const UserInterfaceConfig& cfg);

    /**
     * @brief Destructor
     */
    ~Window();

    /**
     * @brief Window height.
     */
    static int height;

    /**
     * @brief Window width.
     */
    static int width;

    /**
     * @fn update
     * @brief Updates data to display;
     * @param data
     * @return true, if next update needed.
     */
    bool update(info::Data data) noexcept;
  };
} // namespace ui::glfw

