#include "GTextRender.h"

#include "Color.h"
#include "Point.h"
#include "ShaderProgram.h"
#include "ShaderProgramUser.h"

#include <array>
#include <cstdio>
#include <glad/glad.h>
#include <tuple>

namespace ui::glfw {
  GTextRender::GTextRender(FontCharsInfo&& font_chars_info,
                           const ShaderProgram& shader_program)
      : m_shader_program(shader_program)
      , m_font_chars_info(std::forward<FontCharsInfo>(font_chars_info)) {

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    glGenVertexArrays(1, &m_vertex_array_id);
    glGenBuffers(1, &m_vertex_buffer_id);
    glBindVertexArray(m_vertex_array_id);
    glBindBuffer(GL_ARRAY_BUFFER, m_vertex_buffer_id);
    glBufferData(
        GL_ARRAY_BUFFER, sizeof(float) * 6 * 4, nullptr, GL_DYNAMIC_DRAW);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(float), nullptr);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
  }

  RenderedTextMetrics
  GTextRender::render(std::string&& text, Point position, Color color) const {

    ShaderProgramUser user(m_shader_program);
    auto p_id = m_shader_program.id();
    auto text_color_uniform_id = glGetUniformLocation(p_id, "textColor");
    auto [r, g, b] = color;
    glUniform3f(text_color_uniform_id, r, g, b);
    glActiveTexture(GL_TEXTURE0);
    glBindVertexArray(m_vertex_array_id);

    int x = position.x;
    int y = position.y;
    constexpr int scale = 1;
    int text_max_height = 0;
    for (const auto c : text) {
      const auto& char_info = m_font_chars_info.at(c);

      int xpos = x + char_info.bitmap_left * scale;
      int ypos = y - (char_info.bitmap_rows - char_info.bitmap_top) * scale;

      int w = char_info.bitmap_width * scale;
      int h = char_info.bitmap_rows * scale;

      auto [lt_x, lt_y, lt_z] = Point{ xpos, ypos + h, 0 };
      auto [lb_x, lb_y, lb_z] = Point{ xpos, ypos, 0 };
      auto [rb_x, rb_y, rb_z] = Point{ xpos + w, ypos, 0 };
      auto [rt_x, rt_y, rt_z] = Point{ xpos + w, ypos + h, 0 };
      float vertices[6][4] = {
        {static_cast<float>(lt_x), static_cast<float>(lt_y), 0.0f, 0.0f},
        {static_cast<float>(lb_x), static_cast<float>(lb_y), 0.0f, 1.0f},
        {static_cast<float>(rb_x), static_cast<float>(rb_y), 1.0f, 1.0f},

        {static_cast<float>(lt_x), static_cast<float>(lt_y), 0.0f, 0.0f},
        {static_cast<float>(rb_x), static_cast<float>(rb_y), 1.0f, 1.0f},
        {static_cast<float>(rt_x), static_cast<float>(rt_y), 1.0f, 0.0f},
      };

      glBindTexture(GL_TEXTURE_2D, char_info.texture_id);
      glBindBuffer(GL_ARRAY_BUFFER, m_vertex_buffer_id);
      glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vertices), vertices);
      glDrawArrays(GL_TRIANGLES, 0, 6);
      x += (char_info.advance >> 6) * scale;

      text_max_height = text_max_height > h ? text_max_height : h;
    }

    int text_width = x - position.x;

    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
    glBindTexture(GL_TEXTURE_2D, 0);

    return std::make_tuple(text_width, text_max_height);
  }

  GTextRender::~GTextRender() {
    glDisableVertexAttribArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
    glDeleteVertexArrays(1, &m_vertex_array_id);
    glDeleteBuffers(1, &m_vertex_buffer_id);
  }
} // namespace ui::glfw
