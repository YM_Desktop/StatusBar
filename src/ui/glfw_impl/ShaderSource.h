#pragma once

namespace ui::glfw {
  static constexpr char vertex_source[] = R"(
    #version 330 core
    layout (location = 0) in vec4 vertex; // <vec2 pos, vec2 tex>
    out vec2 TexCoords;

    uniform mat4 projection;
    uniform int window_width;
    uniform int window_height;

    void main()
    {
        gl_Position = vec4(vertex.x * 2.0f / window_width - 1.0f, vertex.y * 2.0f / window_height - 1.0f, 0.0, 1.0);
        TexCoords = vertex.zw;
    }  
  )";

  static constexpr char fragment_source[] = R"(
    #version 330 core
    in vec2 TexCoords;
    out vec4 color;

    uniform sampler2D text;
    uniform vec3 textColor;

    void main()
    {
        vec4 sampled = vec4(1.0, 1.0, 1.0, texture(text, TexCoords).r);
        color = vec4(textColor.r/255.0f, textColor.g/255.0f, textColor.b/255.0f, 1.0) * sampled;
    }
  )";

  static constexpr char simple_vertex_source[] = R"(
  #version 330 core
  layout (location = 0) in vec3 vertex;

  uniform int window_width;
  uniform int window_height;

  void main()
  {
    gl_Position = vec4(vertex.x * 2.0f / window_width - 1.0f, vertex.y * 2.0f / window_height - 1.0f, 0.0f, 1.0f);
  }
  )";

  static constexpr char simple_fragment_source[] = R"(
  #version 330 core
  out vec4 figure_color;

  uniform vec3 figure_color_set;

  void main()
  {
    figure_color = vec4(figure_color_set.r/255.0f, figure_color_set.g/255.0f, figure_color_set.b/255.0f, 1.0);
  }
  )";
} // namespace ui::glfw
