#include "FontLoader.h"

#include "FontError.h"
#include "freetype/freetype.h"

#include <array>
#include <glad/glad.h>

namespace {
  std::array err_msg{ 'E', 'r', 'r', 'o', 'r', ' ', 'l', 'o', 'a', 'd',
                      'i', 'n', 'g', ' ', 'c', 'h', 'a', 'r', ' ', ' ' };
}

namespace ui::glfw {
  void FontLoader::verify(bool statement, std::string_view err_msg) const {
    if (!statement)
      throw FontLoaderError(err_msg.data());
  }

  FontCharInfo FontLoader::load_char(unsigned char c) const {

    err_msg.back() = static_cast<char>(c);
    verify(0 == FT_Load_Char(m_face, c, FT_LOAD_RENDER), err_msg.data());

    unsigned int texture;
    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_2D, texture);
    auto width = m_face->glyph->bitmap.width;
    auto rows = m_face->glyph->bitmap.rows;
    auto left = m_face->glyph->bitmap_left;
    auto top = m_face->glyph->bitmap_top;
    unsigned int advance = static_cast<unsigned int>(m_face->glyph->advance.x);
    glTexImage2D(GL_TEXTURE_2D,
                 0,
                 GL_RED,
                 width,
                 rows,
                 0,
                 GL_RED,
                 GL_UNSIGNED_BYTE,
                 m_face->glyph->bitmap.buffer);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    FontCharInfo char_info{ .texture_id = texture,
                            .bitmap_width = width,
                            .bitmap_rows = rows,
                            .bitmap_left = left,
                            .bitmap_top = top,
                            .advance = advance,
                            .buffer = m_face->glyph->bitmap.buffer };
    return char_info;
  }

  FontLoader::FontLoader(std::string_view font_name, std::size_t font_size) {
    verify(0 == FT_Init_FreeType(&m_lib), "Failed to initialise FreeType");
    verify(0 == FT_New_Face(m_lib, font_name.data(), 0, &m_face),
           "Failed to load font");
    FT_Set_Pixel_Sizes(m_face, 0, font_size);

    glActiveTexture(GL_TEXTURE0);
    for (unsigned char c{ 0u }; c < 128; ++c) {
      m_chars.emplace(c, load_char(c));
    }
    glBindTexture(GL_TEXTURE_2D, 0);
  }

  FontLoader::~FontLoader() {
    FT_Done_Face(m_face);
    FT_Done_FreeType(m_lib);
  }

  const FontCharsInfo& FontLoader::chars_info() const& noexcept {
    return m_chars;
  }

  FontCharsInfo FontLoader::chars_info() && noexcept { return m_chars; }
} // namespace ui::glfw
