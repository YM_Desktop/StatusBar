#include "BatteryIndicator.h"

#include "GFilledPolygon.h"
#include "GPolygon.h"
#include "Point.h"
#include "Rectangle.h"
#include "ShaderProgram.h"

#include <algorithm>
#include <cstdio>
#include <string>

namespace {

  auto
  outline_points_from_placement(const ui::glfw::Rectangle& placement) noexcept
      -> ui::glfw::GPolygon<8>::Points {
    int width = placement.width;
    int height = placement.height;

    int left_x = placement.left_bottom.x;
    int right_x = left_x + width;
    int top_right_x = (right_x - 5);

    int bottom_y = placement.left_bottom.y;
    int top_y = bottom_y + height;
    int height_third_part = static_cast<int>(height / 3.0f);

    const int z = placement.left_bottom.z;

    ui::glfw::GPolygon<8>::Points points;
    points.at(0) = placement.left_bottom;
    points.at(1) = { left_x, top_y, z };
    points.at(2) = { top_right_x, top_y, z };
    points.at(3) = { top_right_x, top_y - height_third_part, z };
    points.at(4) = { right_x, top_y - height_third_part, z };
    points.at(5) = { right_x, bottom_y + height_third_part, z };
    points.at(6) = { top_right_x, bottom_y + height_third_part, z };
    points.at(7) = { top_right_x, bottom_y, z };

    return points;
  }

  auto positive_contact_points_from_outline_points(
      const ui::glfw::GPolygon<8>::Points& points) noexcept
      -> std::array<ui::glfw::Point, 2ul> {
    std::array<ui::glfw::Point, 2ul> contact_points = { points.at(6),
                                                        points.at(4) };
    contact_points.at(0).x -= 1;
    return contact_points;
  }

  auto level_points_from_outline_and_percentage(
      const ui::glfw::GPolygon<8>::Points& points,
      int margin,
      unsigned char percentage) noexcept -> std::array<ui::glfw::Point, 2ul> {
    std::array<ui::glfw::Point, 4ul> level_points;
    level_points.at(0) = { .x = points.at(0).x + margin,
                           .y = points.at(0).y + margin };
    level_points.at(1) = { .x = points.at(1).x + margin,
                           .y = points.at(1).y - margin };
    level_points.at(2) = { .x = points.at(2).x - margin,
                           .y = points.at(2).y - margin };
    level_points.at(3) = { .x = points.at(7).x - margin,
                           .y = points.at(7).y + margin };

    int leveled_x =
        level_points.at(0).x +
        (level_points.at(3).x - level_points.at(0).x) * percentage / 100;
    level_points.at(2).x = leveled_x;
    level_points.back().x = leveled_x;

    return { level_points.at(0), level_points.at(2) };
  }

} // namespace

namespace ui::glfw {

  BatteryIndicator::BatteryIndicator(
      const ShaderProgram& shader_program,
      Rectangle&& placement,
      const ::ui::UserInterfaceConfig::Battery::Colors& colors,
      const ::ui::UserInterfaceConfig::Battery& battery_cfg)
      : m_outline(shader_program, outline_points_from_placement(placement))
      , m_positive_contact(shader_program,
                           positive_contact_points_from_outline_points(
                               outline_points_from_placement(placement)))
      , m_level(shader_program,
                level_points_from_outline_and_percentage(
                    outline_points_from_placement(placement),
                    2,
                    100u))
      , m_colors(colors)
      , m_battery_cfg(battery_cfg) {}

  BatteryIndicator::~BatteryIndicator() = default;

  void BatteryIndicator::render(unsigned char percentage,
                                Rectangle&& placement_shifted,
                                int margin) noexcept {

    auto update_outline_points =
        [&placement =
             placement_shifted](GPolygon<8>::Points& points) noexcept -> void {
      int width = placement.width;
      int height = placement.height;

      int left_x = placement.left_bottom.x;
      int right_x = left_x + width;
      int top_right_x = (right_x - 5);

      int bottom_y = placement.left_bottom.y;
      int top_y = bottom_y + height;
      int height_third_part = static_cast<int>(height / 3.0f);

      const int z = placement.left_bottom.z;

      points.at(0) = placement.left_bottom;
      points.at(1) = { left_x, top_y, z };
      points.at(2) = { top_right_x, top_y, z };
      points.at(3) = { top_right_x, top_y - height_third_part, z };
      points.at(4) = { right_x, top_y - height_third_part, z };
      points.at(5) = { right_x, bottom_y + height_third_part, z };
      points.at(6) = { top_right_x, bottom_y + height_third_part, z };
      points.at(7) = { top_right_x, bottom_y, z };
    };

    m_outline.update_vertices(update_outline_points);
    m_outline.render(m_colors.normal);

    auto update_positive_contact_points =
        [&outline_points = m_outline.points()](
            std::array<Point, 4ul>& points) noexcept -> void {
      points = { outline_points.at(6),
                 outline_points.at(3),
                 outline_points.at(4),
                 outline_points.at(5) };
      points.at(0).x -= 1;
      points.at(1).x -= 1;
    };

    m_positive_contact.update_vertices(update_positive_contact_points);
    m_positive_contact.render(m_colors.normal);

    auto update_level_points =
        [&outline_points = m_outline.points(), margin, percentage](
            std::array<Point, 4ul>& points) noexcept -> void {
      points.at(0) = { .x = outline_points.at(0).x + margin,
                       .y = outline_points.at(0).y + margin };
      points.at(1) = { .x = outline_points.at(1).x + margin,
                       .y = outline_points.at(1).y - margin };
      points.at(2) = { .x = outline_points.at(2).x - margin,
                       .y = outline_points.at(2).y - margin };
      points.at(3) = { .x = outline_points.at(7).x - margin,
                       .y = outline_points.at(7).y + margin };

      int leveled_x =
          points.at(0).x + (points.at(3).x - points.at(0).x) * percentage / 100;
      points.at(2).x = leveled_x;
      points.back().x = leveled_x;
    };

    m_level.update_vertices(update_level_points);
    m_level.render(percentage <= m_battery_cfg.critical ? m_colors.critical
                                                        : m_colors.normal);
  }
} // namespace ui::glfw
