#include "GRectangle.h"

#include "Point.h"

namespace {
  using Point = ui::glfw::Point;
  std::array<Point, 4ul>
  filled_rectangle_to_primitive_points(const Point& left_bottom,
                                       const Point& right_top) noexcept {

    Point left_top{ left_bottom.x, right_top.y, 0 };
    Point right_bottom{ right_top.x, left_bottom.y, 0 };

    std::array<Point, 4ul> primitive_points{
      left_bottom, left_top, right_bottom, right_top
    };

    return primitive_points;
  }
} // namespace

namespace ui::glfw {
  GRectangle::GRectangle(const ShaderProgram& shader_program,
                         const std::array<Point, 2ul>& points)
      : Base(shader_program,
             filled_rectangle_to_primitive_points(points.at(0), points.at(1)),
             Indices{ 0, 1, 2, 2, 3, 0 }) {}

  GRectangle::~GRectangle() = default;
} // namespace ui::glfw
