#pragma once
#include "Window.h"

#include <array>

namespace ui::glfw {

  /**
   * @struct Point
   * @brief Represents point.
   */
  struct Point {
    /**
     * @brief x
     */
    int x;

    /**
     * @brief y
     */
    int y;

    /**
     * @brief z
     */
    int z;
  };
} // namespace ui::glfw
