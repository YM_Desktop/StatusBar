#pragma once

#include "Shader.h"

#include <vector>

namespace ui::glfw {
  /**
   * @class ShaderProgram
   * @brief Represents OpenGL graphics program with shaders.
   */
  class ShaderProgram {
    /**
     * @brief Program GL specific id
     */
    GLuint m_id;

    /**
     * @fn validate_link_status
     * @brief verifies link was successful
     * @throws ProgramLinkError
     */
    void validate_link_status() const;

  public:
    /**
     * @brief Constructor
     * @param shaders
     */
    ShaderProgram(Shaders&& shaders);

    /**
     * @brief Destructor
     */
    ~ShaderProgram();

    /**
     * @fn use
     * @brief enables program with shaders
     */
    void use() const noexcept;

    /**
     * @fn disable
     * @brief disables program
     */
    void disable() const noexcept;

    /**
     * @fn id
     * @brief id getter
     * @return id of the program
     */
    GLuint id() const noexcept;
  };
} // namespace ui::glfw
