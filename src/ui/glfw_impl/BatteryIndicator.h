#pragma once
#include "GFilledPolygon.h"
#include "GPolygon.h"
#include "GRectangle.h"
#include "Rectangle.h"

#include <UserInterfaceConfig.h>

namespace ui::glfw {

  class ShaderProgram;
  class GTextRender;

  /**
   * @class BatteryIndicator
   * @brief Handles battery indicator display.
   */
  class BatteryIndicator {
  public:
    /**
     * @brief Constructor
     * @param shader_program
     * @param placement
     * @param colors
     * @param battery_cfg
     */
    BatteryIndicator(const ShaderProgram& shader_program, Rectangle&& placement,
                     const ::ui::UserInterfaceConfig::Battery::Colors& colors,
                     const ::ui::UserInterfaceConfig::Battery& battery_cfg);

    /**
     * @brief Destructor
     */
    ~BatteryIndicator();

    /**
     * @fn render
     * @brief Renders battery level on screen.
     * @param percentage: battery percentage to display.
     * @param placement_shifted: placement shifted from text
     * @param margin: margin between outline and filled part.
     */
    void render(unsigned char percentage, Rectangle&& placement_shifted,
                int margin) noexcept;

  private:
    /**
     * @brief Battery indicator polygon outline.
     */
    GPolygon<8> m_outline;

    /**
     * @brief Battery indicator positive contact rectangle.
     */
    GRectangle m_positive_contact;

    /**
     * @brief Current battery level visually.
     */
    GRectangle m_level;

    /**
     * @brief Color scheme.
     */
    const UserInterfaceConfig::Battery::Colors& m_colors;

    /**
     * @brief Battery info config.
     */
    const UserInterfaceConfig::Battery& m_battery_cfg;
  };
} // namespace ui::glfw
