#pragma once
#include <stdexcept>

namespace ui::glfw {
  /**
   * @struct EnvironmentError
   * @brief Custom error for environment issue
   */
  struct EnvironmentError : public std::runtime_error {
    /**
     * @brief Constructor
     * @param err_msg: the error message
     */
    EnvironmentError(const char* err_msg)
        : std::runtime_error(err_msg) {}
  };
} // namespace ui::glfw
