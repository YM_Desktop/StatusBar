#pragma once
#include "GPrimitiveRender.h"

#include <array>

namespace ui::glfw {

  class ShaderProgram;

  /**
   * @class GFilledPolygon
   * @brief Draws polygon and manages OpenGL handlers.
   * @tparam NumberOfPoints
   * @tparam NumberOfIndices
   */
  template <std::size_t NumberOfPoints, std::size_t NumberOfIndices>
  class GFilledPolygon
      : private GPrimitiveRender<NumberOfPoints, NumberOfIndices> {

  public:
    /**
     * @brief Alias for base class.
     */
    using Base = GPrimitiveRender<NumberOfPoints, NumberOfIndices>;

    /**
     * @brief Alias for polygon points type.
     */
    using Points = std::array<Point, NumberOfPoints>;

    /**
     * @brief Alias for points indices.
     */
    using Indices = std::array<unsigned int, NumberOfIndices>;

    /**
     * @brief Alias for vertices updater type.
     */
    using VerticesUpdater = std::function<void(Points&)>;

  public:
    /**
     * @brief Constructor
     */
    GFilledPolygon(
        const ShaderProgram& shader_program, const Points& points,
        Indices&& indices = []() -> Indices {
          static_assert(NumberOfPoints <= NumberOfIndices,
                        "Indices array overlaps points array");
          Indices inds;
          for (unsigned int v = 0u; auto& i : inds) {
            i = v++;
          }
          return inds;
        }())
        : Base(shader_program, GL_TRIANGLES, points, indices) {}

    /**
     * @brief Destructor
     */
    ~GFilledPolygon() = default;

    /**
     * @brief Updates color of primitive
     */
    using Base::update_color;

    /**
     * @brief renders triangle
     */
    using Base::render;

    /**
     * @brief updates polygon vertices
     */
    using Base::update_vertices;

    /**
     * @brief returns points of polygon
     */
    using Base::points;
  };

} // namespace ui::glfw
