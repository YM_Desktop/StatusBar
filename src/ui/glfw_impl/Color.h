#pragma once
#include <array>

namespace ui::glfw {

  /**
   * @struct Color
   * @brief Represents color in RGB mode.
   */
  struct Color {
    /**
     * brief red part
     */
    unsigned char r;

    /**
     * brief green part
     */
    unsigned char g;

    /**
     * brief blue part
     */
    unsigned char b;
  };
} // namespace ui::glfw

