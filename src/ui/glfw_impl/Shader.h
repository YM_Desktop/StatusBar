#pragma once
#include <string_view>
#include <vector>

extern "C" {
typedef unsigned int GLuint;
typedef unsigned int GLenum;
}

namespace ui::glfw {
  /**
   * @class Shader
   * @brief Represents OpenGL shader
   */
  class Shader {
    /**
     * @brief shader GL specific id
     */
    GLuint m_id;

    /**
     * @fn validate_compilation_status
     * @brief validates compilation was successful
     * @throws ShaderCompilationError
     */
    void validate_compilation_status() const;

  public:
    /**
     * @brief Constructor
     * @param source: source code for shader
     * @param shader_type: GL specific type of shader, e.g. GL_VERTEX_SHADER
     */
    Shader(std::string_view source, GLenum shader_type);

    /**
     * @brief Destructor
     */
    ~Shader();

    /**
     * @fn id
     * @return shader id
     */
    GLuint id() const noexcept;
  };

  using Shaders = std::vector<Shader>;
} // namespace ui::glfw
