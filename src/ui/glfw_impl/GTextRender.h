#pragma once
#include "Color.h"
#include "FontCharInfo.h"
#include "Point.h"
#include "ShaderProgram.h"
#include "ShaderSource.h"

#include <string>
#include <tuple>

namespace ui::glfw {

  class ShaderProgram;
  using RenderedTextMetrics = std::tuple<int, int>;

  /**
   * @class GTextRender
   * @brief renders text given
   */
  class GTextRender {
    /**
     * @brief A shader program to use for rendering.
     */
    ShaderProgram m_shader_program;

    /**
     * @brief Font chars glyphs data
     */
    FontCharsInfo m_font_chars_info;

    /**
     * @brief vertex array id
     */
    unsigned int m_vertex_array_id;

    /**
     * @brief vertex buffer id
     */
    unsigned int m_vertex_buffer_id;

  public:
    /**
     * @brief Constructor
     * @param font_chars_info: Font chars glyphs data
     * @param shader_program: Shader program to for rendering glyps.
     */
    GTextRender(FontCharsInfo&& font_chars_info,
                const ShaderProgram& shader_program);

    /**
     * @brief Destructor
     */
    ~GTextRender();

    /**
     * @fn render
     * @brief renders text
     * @param text: text string to render
     * @param position: position of first char on the screen
     * @param color: text color
     * @return tuple with graphical text width and text max height
     */
    RenderedTextMetrics render(std::string&& text, Point position,
                               Color color) const;
  };
} // namespace ui::glfw
