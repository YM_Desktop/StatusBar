#pragma once
#include <filesystem>
#include <optional>

/**
 * @struct ParsedArgs
 * @brief Contains args parsed from CLI.
 */
struct ParsedArgs {

  /**
   * @brief Path to config file.
   */
  std::optional<std::filesystem::path> config_path;

  /**
   * @brief Constructor
   * @param argc: number of arguments, including program name.
   * @param argv: Array of arguments.
   */
  ParsedArgs(int argc, char** argv);

  /**
   * @brief Destructor
   */
  ~ParsedArgs();
};
