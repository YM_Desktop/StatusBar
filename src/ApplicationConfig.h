#pragma once
#include "info/InfoAggregatorConfig.h"
#include "info/InfoID.h"
#include "ui/UserInterfaceConfig.h"

#include <filesystem>

/**
 * @struct ApplicationConfig
 * Contains parsed application configuration.
 */
struct ApplicationConfig {
  /**
   * @brief Info aggregator config.
   */
  info::InfoAggregatorConfig info_aggregator;

  /**
   * @brief user interface configuration instance
   */
  ui::UserInterfaceConfig user_interface;

  /**
   * @fn from
   * @brief Creates ApplicationConfig instance from file specified.
   * @param config_file: file path to config
   * @return ApplicationConfig
   */
  static ApplicationConfig from(std::filesystem::path&& config_file);
};

