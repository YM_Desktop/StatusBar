#include "Application.h"

#include "ApplicationConfig.h"

Application::Application(const ApplicationConfig& config)
    : m_ui(config.user_interface)
    , m_aggregator(config.info_aggregator) {
  info::Data data;
  do {
    data = m_aggregator.aggregate();
  } while (m_ui.update(data));
}
