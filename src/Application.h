#pragma once
#include "info/InfoAggregator.h"
#include "ui/UserInterface.h"

struct ApplicationConfig;

/**
 * @class Application
 * Represents Application instance.
 */
class Application {
  /**
   * @brief UserInterface instance.
   */
  ui::UserInterface m_ui;

  /**
   * @brief InfoAggregator instance.
   */
  info::InfoAggregator m_aggregator;

public:
  /**
   * @brief Constructor.
   * @param config: ApplicationConfig
   */
  Application(const ApplicationConfig& config);

  /**
   * @brief Destructor.
   */
  ~Application() = default;
};
