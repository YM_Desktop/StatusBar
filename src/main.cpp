#include "Application.h"
#include "ApplicationConfig.h"
#include "ParsedArgs.h"

int main(int argc, char** argv) {
  ParsedArgs pargs{ argc, argv };
  ApplicationConfig app_cfg =
      !pargs.config_path
          ? ApplicationConfig{}
          : ApplicationConfig::from(std::move(pargs.config_path.value()));

  Application app(app_cfg);
  return 0;
}
