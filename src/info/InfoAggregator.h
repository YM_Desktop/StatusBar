#pragma once

#include <thread>

namespace info {

  struct TimeData;
  struct UptimeData;
  struct BatteryData;
  struct CpuLoadData;
  struct MemLoadData;
  struct Data;

  struct InfoAggregatorConfig;

  /**
   * @class InfoAggregator
   * Holds and provides data from data storage filled by InfoPool.
   */
  class InfoAggregator {

    /**
     * @brief Number of cpus on system.
     */
    const unsigned int m_number_of_cpus;

    /**
     * @brief Config reference.
     */
    const InfoAggregatorConfig& m_cfg;

    /**
     * @fn current_time
     * @brief Gets current system time
     * @return TimeData
     */
    TimeData current_time() const noexcept;

    /**
     * @fn uptime
     * @brief Current system uptime
     * @return UptimeData
     */
    UptimeData uptime() const noexcept;

    /**
     * @fn battery_data
     * @brief Current battery data
     * @return BatteryData
     */
    BatteryData battery_data() const noexcept;

    /**
     * @fn cpu_load
     * @brief Current cpu load level
     * @return CpuLoadData
     */
    CpuLoadData cpu_load() const noexcept;

    /**
     * @fn mem_load
     * @brief Current mem load level
     * @return MemLoadData
     */
    MemLoadData mem_load() const noexcept;

  public:
    /**
     * @brief Constructor
     */
    InfoAggregator(const InfoAggregatorConfig& cfg);

    /**
     * @brief Destructor
     */
    ~InfoAggregator();

    /**
     * @fn aggregate
     * @return Gathered data copy.
     */
    Data aggregate() const noexcept;
  };
} // namespace info
