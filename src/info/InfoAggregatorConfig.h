#pragma once
#include <filesystem>

namespace info {

  /**
   * @struct InfoAggregatorConfig
   * @brief Config for data aggregation
   */
  struct InfoAggregatorConfig {

    /**
     * @struct Uptime
     * @brief Uptime data configuration
     */
    struct Uptime {
      /**
       * @brief Uptime data file.
       */
      std::filesystem::path file{"/proc/uptime"};
    }
    /**
     * @brief Uptime config instance.
     */
    uptime;

    /**
     * @struct Battery
     * @brief Contains battery data file paths.
     */
    struct Battery {
      /**
       * @brief Battery level data file.
       */
      std::filesystem::path level{"/sys/class/power_supply/BAT0/capacity"};

      /**
       * @brief Battery status data file.
       */
      std::filesystem::path status{"/sys/class/power_supply/BAT0/status"};
    }
    /**
     * @brief battery data files.
     */
    battery;

    /**
     * @struct Cpu
     * @brief Cpu data configuration.
     */
    struct Cpu {
      /**
       * @brief cpu data file.
       */
      std::filesystem::path file{"/proc/stat"};
    }
    /**
     * @brief Cpu config instance.
     */
    cpu;

    /**
     * @struct Memory
     * @brief Memory data configuration.
     */
    struct Memory {
      /**
       * @brief memory data file.
       */
      std::filesystem::path file{"/proc/meminfo"};
    }
    /**
     * @brief Memory configuration instance.
     */
    memory;
  };
} // namespace info
