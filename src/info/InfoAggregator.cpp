#include "InfoAggregator.h"

#include "BatteryData.h"
#include "CpuLoadData.h"
#include "InfoAggregatorConfig.h"

#include <Data.h>
#include <algorithm>
#include <array>
#include <chrono>
#include <fstream>
#include <optional>
#include <thread>
#include <utility>
#include <vector>

namespace {
  std::vector<std::array<long, 4ul>>
  get_cpu_stats(std::string_view cpu_stats_file,
                std::size_t number_of_cpus) noexcept {
    std::vector<std::array<long, 4ul>> stats;
    stats.reserve(number_of_cpus);

    std::ifstream cpu_stats_stream(cpu_stats_file.data());

    for (std::size_t core = 0ul; core < number_of_cpus; ++core) {
      std::string _;
      long a, b, c, idle;
      cpu_stats_stream >> _ >> a >> b >> c >> idle >> _ >> _ >> _ >> _ >> _ >>
          _;
      stats.emplace_back(std::array{ a, b, c, idle });
    }

    return stats;
  }
} // namespace

namespace info {

  TimeData InfoAggregator::current_time() const noexcept {
    using namespace std::chrono;
    std::time_t now_c = system_clock::to_time_t(system_clock::now());
    return { .timestamp = *std::localtime(&now_c) };
  }

  UptimeData InfoAggregator::uptime() const noexcept {
    /*
     * Format is: <uptime seconds> <summary idle time for cores>
     * We need only first part before space.
     */
    std::string uptime_raw;
    std::ifstream(m_cfg.uptime.file.c_str()) >> uptime_raw;
    float uptime = std::stof(uptime_raw);
    return { .uptime = std::chrono::seconds(static_cast<long>(uptime)) };
  }

  BatteryData InfoAggregator::battery_data() const noexcept {
    if (not std::filesystem::exists(m_cfg.battery.level) or
        not std::filesystem::exists(m_cfg.battery.status)) {
      return { .level = 100u, .status = BatteryStatus::NO_BATTERY };
    }

    std::string battery_level_raw;
    std::ifstream(m_cfg.battery.level) >> battery_level_raw;
    unsigned char battery_level = std::stoi(battery_level_raw);

    std::string battery_status_raw;
    std::ifstream(m_cfg.battery.status) >> battery_status_raw;
    BatteryStatus battery_status =
        battery_status_raw == "Discharging" ? BatteryStatus::DISCHARGING
        : battery_status_raw == "Charging"  ? BatteryStatus::CHARGING
                                            : BatteryStatus::FULL;
    return { .level = battery_level, .status = battery_status };
  }

  CpuLoadData InfoAggregator::cpu_load() const noexcept {
    /*
     * First goes average value for all cpus.
     * Thus we need number_of_cpus + 1 records.
     */
    auto prev_cpu_stats =
        get_cpu_stats(m_cfg.cpu.file.c_str(), m_number_of_cpus + 1);

    std::vector<std::array<long, 2ul>> prev_totals;
    prev_totals.reserve(m_number_of_cpus + 1ul);
    std::transform(prev_cpu_stats.begin(),
                   prev_cpu_stats.end(),
                   std::back_inserter(prev_totals),
                   [](const auto& cpu_stats) noexcept -> std::array<long, 2ul> {
                     auto [a, b, c, idle] = cpu_stats;
                     return std::array{ a + b + c + idle, idle };
                   });

    std::this_thread::sleep_for(std::chrono::milliseconds(500u));

    auto cpu_stats =
        get_cpu_stats(m_cfg.cpu.file.c_str(), m_number_of_cpus + 1);
    std::vector<std::array<long, 2ul>> totals;
    totals.reserve(m_number_of_cpus + 1ul);
    std::transform(cpu_stats.begin(),
                   cpu_stats.end(),
                   std::back_inserter(totals),
                   [](const auto& cpu_stats) noexcept -> std::array<long, 2ul> {
                     auto [a, b, c, idle] = cpu_stats;
                     return std::array{ a + b + c + idle, idle };
                   });

    decltype(CpuLoadData::cpu_levels) levels;
    levels.reserve(cpu_stats.size());
    auto prev_totals_it = prev_totals.begin();
    auto totals_it = totals.begin();
    while (prev_totals_it != prev_totals.end() || totals_it != totals.end()) {
      auto [pt, pidle] = *prev_totals_it;
      auto [t, idle] = *totals_it;
      auto total_diff = t - pt;
      levels.push_back(
          !total_diff ? static_cast<unsigned char>(0u)
                      : static_cast<unsigned char>(
                            100u * (total_diff - idle + pidle) / total_diff));
      prev_totals_it = std::next(prev_totals_it);
      totals_it = std::next(totals_it);
    }

    return { .level = levels.at(0),
             .cpu_levels =
                 std::vector(std::next(levels.begin()), levels.end()) };
  }

  MemLoadData InfoAggregator::mem_load() const noexcept {
    unsigned long total_memory, available_memory;
    std::string _;
    std::ifstream(m_cfg.memory.file) >> _ >> total_memory >> _ >> _ >> _ >> _ >>
        _ >> available_memory;

    unsigned long used_memory = total_memory - available_memory;

    return { .level =
                 static_cast<unsigned char>(100u * used_memory / total_memory),
             .megabytes = static_cast<unsigned int>(used_memory / 1024ul) };
  }

  InfoAggregator::InfoAggregator(const InfoAggregatorConfig& cfg)
      : m_number_of_cpus(std::thread::hardware_concurrency())
      , m_cfg(cfg) {}

  InfoAggregator::~InfoAggregator() = default;

  Data InfoAggregator::aggregate() const noexcept {
    Data data{ .time = current_time(),
               .battery = battery_data(),
               .cpu = cpu_load(),
               .memory = mem_load(),
               .uptime = uptime() };
    return data;
  }
} // namespace info
