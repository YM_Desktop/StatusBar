#pragma once

namespace info {
  enum class InfoID { TIME, DATE, CPU, MEM, NET, BATTERY };
}
