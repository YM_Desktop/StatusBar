command! Init  :!cmake -S . -B debug -DCMAKE_BUILD_TYPE=Debug -DCMAKE_INSTALL_PREFIX=debug -DBUILD_DOC=TRUE
command! Build :!cmake --build debug --parallel && cmake --install debug
command! Test  :!LD_LIBRARY_PATH=${PWD}/debug/lib ctest --test-dir debug/test
command! TestV :!LD_LIBRARY_PATH=${PWD}/debug/lib ctest --test-dir debug/test --verbose
command! Run   :!./debug/bin/StatusBar
command! CBuild :!cmake --build debug --target clean && cmake --build debug --parallel && cmake --install debug
command! -nargs=1 TBuild :!cmake --build debug --target <f-args> -j `nproc` 
command! -nargs=1 TTest :!LD_LIBRARY_PATH=${PWD}/build/lib ctest --test-dir debug/test -R <f-args>
command! -nargs=1 TTestV :!LD_LIBRARY_PATH=${PWD}/build/lib ctest --test-dir debug/test -R <f-args> --verbose
